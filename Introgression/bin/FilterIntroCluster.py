#!/usr/bin/env python
"""This script is used to filter the introgressed gene in ortholog clusters"""
"""Introgressed genes are defined by two levels and determined
   by number of each level"""

import sys
import csv

CRYPTOS = ["JEC21","WM276","R265"]
SIMBOLS = ["@","?","%"]

if(len(sys.argv) < 2):
    print ("Usage: %s <Introgress_table>" %(sys.argv[0]))
    sys.exit(1)

Infile = sys.argv[1]
fh_Infile = open(Infile, 'r')
reader = csv.reader(fh_Infile, delimiter = '\t')
next(reader)
for line in reader:
    if (int(line[1]) < 1): #skip clusters without H99 ortholog
        continue
    level1 = 0
    level2 = 0
    straincount = 0
    for col in range(2,len(line)):
        speinfo = line[col].split(':')
        level1count = 0
        level2count = 0
        isIntro = False
        for info in speinfo:
            if(info == "-" or info[-1] == "*" or info == "x"):
                continue
            else:
                if(info[-1] in SIMBOLS):
                    if(info[-1] == "?"):
                        level2count += 1
                    elif(info[-1] == "@"):
                        level1count += 1
                    elif(info[-1] == "%"):
                        level2count += 1
                else:
                    if(info in CRYPTOS):
                        level1count += 1
                    else:
                        level2count += 1
                if(not isIntro):
                    straincount += 1
                    isIntro = True

        if(level1count == 0 and level2count == 0):
            continue
        
        if(level1count >= level2count):
            level1 += 1
        else:
            level2 += 1
    if(level1 == 0 and level2 == 0):
        continue
    if(level1 >= level2):
        print (line[0] + "\t1\t" + str(straincount))
    else:
        print (line[0] + "\t2\t" + str(straincount))

fh_Infile.close()
sys.exit(0)
