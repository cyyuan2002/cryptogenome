#!/usr/bin/env python

from Bio.Blast import NCBIStandalone
import sys
import argparse

def sortHSPs(Hsps,evalue):
    sorted_hsp = []
    s_index = 0
    while(len(Hsps) > 0):
        for i in range(len(Hsps)):
            if(Hsps[s_index].expect > float(evalue)):
                del Hsps[i]
                break

            if(int(Hsps[i].query_start) < Hsps[s_index].query_start):
                s_index = i
        if(len(Hsps) > 0):
            sorted_hsp.append(Hsps[s_index])
            del Hsps[s_index]
            s_index = 0
    return sorted_hsp


MAX_OVER_LAP = 20

parser = argparse.ArgumentParser()

parser.add_argument("-i","--in",dest='Blast_File',required=True,help='input file (BLAST plain output format)')
parser.add_argument("-o","--out",dest='Output_File',required=True,help='output file')
parser.add_argument("-I","--identity",dest="Identity",default=0,help='minimum identity for alignment, default 0')
parser.add_argument("-c","--coverage",dest="Coverage",default=0,help='mininum query coverage for alignment, default 0')
parser.add_argument("-l","--length",dest="Length",default=0,help='mininum alignment length for alignment, default 0')
parser.add_argument("-e","--evalue",dest="Evalue",default=10, help='e-value cutoff for each hit, default 10')
parser.add_argument("-H","--head",dest="Print_Head",action='store_true',help='print header in output file')
parser.add_argument("-b","--best",dest="Only_Best",action='store_true',help='best alignment for each query')

args = parser.parse_args()
#print(args)

Fh_Blast = open(args.Blast_File,'r')
Fh_Output = open(args.Output_File,'w')
blast_parser = NCBIStandalone.BlastParser()
blast_iterator = NCBIStandalone.Iterator(Fh_Blast,blast_parser)

if(args.Print_Head):
    Header = "qName\ttName\t\tStrand\tqSize\tqStart\tqEnd\ttStart\ttEnd\ttLength\tIdentity\tCoverage\tqBlocks\ttBlocks\n"
    Fh_Output.write(Header)

for blast_record in blast_iterator:
    align_count = 0
    for alignment in blast_record.alignments:
        if(args.Only_Best):
            if(align_count > 0):
                break
        align_count += 1
        querynames = blast_record.query.split(' ')
        query_name = querynames[0]

        subname = alignment.title.replace('>','')
        subnames = subname.split(' ')
        sbjct_name = subnames[0]
        query_length = int(blast_record.query_letters)
        sbjct_length = int(alignment.length)
        hsp_info = {}
        sorted_hsps = []
        if(len(alignment.hsps) < 2):
            hsp = alignment.hsps[0]
            if(hsp.expect > float(args.Evalue)):
                next;
            hsp_info['match_length'] = int(hsp.identities[0])
            hsp_info['align_length'] = int(hsp.identities[1])
            hsp_info['query_align_length'] = int(hsp.query_end) - int(hsp.query_start) + 1
            hsp_info['query_start'] = int(hsp.query_start)
            hsp_info['query_end'] = int(hsp.query_end)
            hsp_info['sbjct_start'] = int(hsp.sbjct_start)
            hsp_info['sbjct_end'] = int(hsp.sbjct_end)
            hsp_info['query_alignments'] = str(hsp.query_start)+'-'+str(hsp.query_end)
            hsp_info['sbjct_alignments'] = str(hsp.sbjct_start)+'-'+str(hsp.sbjct_end)
            if(hsp.strand[1] == 'Minus'):
                hsp_info['strand'] = '-'
                hsp_info['sbjct_start'], hsp_info['sbjct_end'] = hsp_info['sbjct_end'], hsp_info['sbjct_start']
            else:
                hsp_info['strand'] = '+'
        else:
            sorted_hsps = sortHSPs(alignment.hsps,args.Evalue)
            if(len(sorted_hsps) < 1):
                continue
            hsp_info['query_start'] = int(sorted_hsps[0].query_start)
            hsp_info['query_end'] = int(sorted_hsps[0].query_end)
            hsp_info['sbjct_start'] = int(sorted_hsps[0].sbjct_start)
            hsp_info['sbjct_end'] = int(sorted_hsps[0].sbjct_end)
            hsp_info['query_align_length'] = int(sorted_hsps[0].query_end) - int(sorted_hsps[0].query_start) + 1
            hsp_info['match_length'] = int(sorted_hsps[0].identities[0])
            hsp_info['align_length'] = int(sorted_hsps[0].identities[1])
            hsp_info['query_alignments'] = str(sorted_hsps[0].query_start)+'-'+str(sorted_hsps[0].query_end)
            hsp_info['sbjct_alignments'] = str(sorted_hsps[0].sbjct_start)+'-'+str(sorted_hsps[0].sbjct_end)
            if(sorted_hsps[0].strand[1] == 'Minus'):
                hsp_info['strand'] = '-'
            else:
                hsp_info['strand'] = '+'

            for i in range(1,len(sorted_hsps)):
                hsp_query_length = int(sorted_hsps[i].query_end) - int(sorted_hsps[i].query_start) + 1
                if(sorted_hsps[i].query_start < hsp_info['query_end']):
                    if(sorted_hsps[i].query_end > hsp_info['query_end']):
                        #remove overlapped hsp
                        if((hsp_info['query_end'] - sorted_hsps[i].query_start)/hsp_query_length >= 0.5): 
                            continue
                        hsp_info['query_align_length'] += int(sorted_hsps[i].query_end) - hsp_info['query_end']
                        hsp_info['query_end'] = sorted_hsps[i].query_end
                    else:
                        continue
                else:
                    hsp_info['query_align_length'] += hsp_query_length
                    hsp_info['query_end'] = sorted_hsps[i].query_end
                    
                hsp_info['match_length'] += int(sorted_hsps[i].identities[0])
                hsp_info['align_length'] += int(sorted_hsps[i].identities[1])
                hsp_info['query_alignments'] += ','+str(sorted_hsps[i].query_start)+'-'+str(sorted_hsps[i].query_end)
                hsp_info['sbjct_alignments'] += ','+str(sorted_hsps[i].sbjct_start)+'-'+str(sorted_hsps[i].sbjct_end)
               
                hsp_info['sbjct_end'] = sorted_hsps[i].sbjct_end

            if(hsp_info['strand'] == '-'):
                hsp_info['sbjct_start'], hsp_info['sbjct_end'] = hsp_info['sbjct_end'], hsp_info['sbjct_start']
        identity = hsp_info['match_length'] / hsp_info['align_length'] * 100
        identity = "%.2f" % identity
        alignment_percent = hsp_info['query_align_length'] / query_length * 100
        alignment_percent = "%.2f" %  alignment_percent

        if(int(args.Coverage) > float(alignment_percent)):
            next
        if(int(args.Length) > hsp_info['query_align_length']):
            next
        if(int(args.Identity) > float(identity)):
            next;

        output = query_name + "\t" + sbjct_name + "\t" + hsp_info['strand'] + "\t" + str(query_length) + "\t" + \
                 str(hsp_info['query_start']) + "\t" + str(hsp_info['query_end']) + "\t" + str(hsp_info['sbjct_start']) + \
                 "\t" + str(hsp_info['sbjct_end']) + "\t" + str(sbjct_length) + "\t" + str(identity) + "\t" + \
                 str(alignment_percent) + "\t" + hsp_info['query_alignments'] + "\t" + hsp_info['sbjct_alignments']
        Fh_Output.write(output+"\n")
#        print (output)
