#!/usr/bin/env python
import sys
import csv
import copy
import re

CONVERT_H99 = ["Cryptococcus neoformans var. grubii H99", "Cryptococcus neoformans var. grubii"]
CONVERT_Other = {"Cryptococcus neoformans var. neoformans B-3501A":"JEC21","Cryptococcus neoformans var. neoformans":"JEC21", \
                 "Cryptococcus neoformans var. neoformans JEC21":"JEC21","Cryptococcus gattii WM276":"WM276", \
                 "Cryptococcus gattii":"WM276"}
IdenSimbols = ['*','@','%']

REF_SPECIES = "CNA2"

if(len(sys.argv) < 2):
    raise ValueError ("Usage:%s <Orthologs> <Strain1_BlastParser> <Strain2_BlastParser> ..." %(sys.argv[0]))

OrthFile = sys.argv[1]

Orthologs = {}

with open(OrthFile,'r') as csv_OrthFile:
    reader = csv.reader(csv_OrthFile, delimiter='\t')
    lastClusterID = ''
    OrthoCluster = {}
    for line in reader:
        if(len(line) < 1):
            continue
        if(lastClusterID != line[0]):
            if(lastClusterID != ''):
                Orthologs[lastClusterID] = OrthoCluster
            OrthoCluster = {}
            lastClusterID = line[0]
        if(line[1] not in OrthoCluster):
            OrthoCluster[line[1]] = line[4]
        else:
            OrthoCluster[line[1]] += ":" + line[4]
        

    Orthologs[lastClusterID] = OrthoCluster

strains = []

AllResults = {}

for file in sys.argv[2:]:
    strainN = re.findall(r'Cryp_neof_\S+_V1',file)
    strains.append(strainN[0])
    strainResults = {}
    with open (file,'r') as csv_StrainFile:
        reader = csv.reader(csv_StrainFile, delimiter='\t')
        for line in reader:
            spe = line[1]
            if(len(spe) > 1):
                simbol = ""
                if(spe[-1] in IdenSimbols):
                    simbol = spe[-1]
                    spe = spe[:-1]
                if(spe in CONVERT_H99):
                    if(simbol != ""):
                        spe = simbol
                    else:
                        spe = '-'
                elif(spe in CONVERT_Other):
                    spe = CONVERT_Other[spe] + simbol
                else:
                    spe = spe + simbol
            strainResults[line[0]] = spe
    AllResults[strainN[0]] = strainResults

Header = "Cluster_ID\tH99CopyNum\t" + "\t".join(strains)
print (Header)

sortcluster = sorted(Orthologs.keys())

for cluster in sortcluster:
    clusterinfo = Orthologs[cluster]
    Outline = cluster
    if(REF_SPECIES not in clusterinfo):
        Outline += "\t0"
    else:
        GeneNum = len(clusterinfo[REF_SPECIES].split(':'))
        Outline += "\t" + str(GeneNum)
    for strain in strains:
        if(strain not in clusterinfo):
            Outline += '\tx'
        else:
            geneids = clusterinfo[strain].split(':')
            strainbls = AllResults[strain]
            StrainInfo = ''
            for gene in geneids:
                if(StrainInfo == ''):
                    StrainInfo = strainbls[gene]
                else:
                    StrainInfo += ':'+strainbls[gene]
            Outline += '\t'+StrainInfo

    print (Outline)

