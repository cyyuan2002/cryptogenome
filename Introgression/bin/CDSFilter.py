#!/usr/bin/env python
from Bio import SeqIO
from Bio.SeqRecord import SeqRecord
import sys

if(len(sys.argv) < 2):
    raise ValueError("Usage:%s <CDS_File>" %(sys.argv[0]))


lastGeneID = ""
seqRecord = {}
fh_filein = open (sys.argv[1],'rU')
for record in SeqIO.parse(fh_filein,"fasta"):
    seqinfo = record.description.split(" ")
    GeneID = seqinfo[1]
    if(GeneID in seqRecord):
        if(len(record) > len(seqRecord[GeneID])):
            seqRecord[GeneID] = record
    else: seqRecord[GeneID] = record
fh_filein.close()

for gid in seqRecord:
    record = seqRecord[gid]
    newrec = SeqRecord(record.seq,id=gid)
    print (newrec.format("fasta"))

sys.exit(0)