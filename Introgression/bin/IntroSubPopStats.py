#!/usr/bin/env python

import sys
import csv

STRAINS = ["cng10", "MW_RSA852", "C45", "cng9", "Bt85", "Tu401_1", "Bt63", \
           "Ze90_1", "cng11", "Bt206", "Bt1", "Bt15", "Bt120", "A1-35-8", "H99", \
           "C23", "AD2_60a", "cng8", "AD1_7a", "D17_1", "A2_102_5", "Tu259_1", \
           "CHC193", "A5-35-17", "MW_RSA1955",  "C8", "AD1_83a", "MW_RSA36", \
           "Gb118", "Br795", "cng7", "cng2", "cng3", "cng6", "cng5", "cng4", \
           "cng1", "Th84", "125_91"]

VNII = ["cng10", "MW_RSA852", "C45", "cng9"]
VNB = ["Bt85", "Tu401_1", "Bt63", "Ze90_1", "cng11", "Bt206", "Bt1"]
VNI = ["Bt15", "Bt120", "A1-35-8", "H99", "C23", "AD2_60a", "cng8", "AD1_7a", \
       "D17_1", "A2_102_5", "Tu259_1", "CHC193", "A5-35-17", "MW_RSA1955", "C8", \
       "AD1_83a", "MW_RSA36", "Gb118", "Br795", "cng7", "cng2", "cng3", \
       "cng6", "cng5", "cng4", "cng1", "Th84", "125_91"]

CRYPTOS = ["JEC21","WM276","R265"]
SIMBOLS = ["@","?","%"]

Infile = sys.argv[1]
fh_file = open(Infile,'r')
reader = csv.reader(fh_file,delimiter = "\t")

header = next(reader)

VNBCluster = {}
VNICluster = {}
VNIICluster = {}
FltCluster = []
Introlevel = {}
H99CopyNum = {}

for line in reader:
    if (int(line[1]) < 1): #skip clusters without H99 ortholog
        continue
    level1 = 0
    level2 = 0
    straincount = 0
    H99CopyNum[line[0]] = line[1]
    VNICluster[line[0]] = 0
    VNBCluster[line[0]] = 0
    VNIICluster[line[0]] = 0
    for col in range(2, len(line)):
        speinfo = line[col].split(':')
        strain = header[col]
        strain = strain.replace("Cryp_neof_","")
        strain = strain.replace("grubii_","")
        strain = strain.replace("_V1","")
        level1count = 0
        level2count = 0
        isIntro = False
        for info in speinfo:
            if(info == "-" or info[-1] == "*" or info == "x"):
                continue
            else:
                if(line[0] not in FltCluster):
                    FltCluster.append(line[0])
                if(info[-1] in SIMBOLS):
                    if(info[-1] == "?"):
                        level2count += 1
                    elif(info[-1] == "@"):
                        level1count += 1
                    elif(info[-1] == "%"):
                        level2count += 1
                else:
                    if(info in CRYPTOS):
                        level1count += 1
                    else:
                        level2count += 1
                if(not isIntro):
                    straincount += 1
                    isIntro = True
        if(level1count == 0 and level2count == 0):
            continue

        if(strain in VNI):
            VNICluster[line[0]] += 1
        elif(strain in VNII):
            VNIICluster[line[0]] += 1
        elif(strain in VNB):
            VNBCluster[line[0]] += 1

        if(level1count >= level2count):
            level1 += 1
        else:
            level2 += 1
    if(level1 == 0 and level2 == 0):
        continue
    if(level1 >= level2):
        Introlevel[line[0]] = "1"
    else:
        Introlevel[line[0]] = "2"

fh_file.close()

print ("ClusterID\tH99\tType\tVNI\tVNB\tVNII\tSum")
for cluster in FltCluster:
    SumIntro = VNICluster[cluster] + VNBCluster[cluster] + VNIICluster[cluster]
    Outline = ("%s\t%s\t%s\t%s\t%s\t%s\t%s" %(cluster, H99CopyNum[cluster], \
                Introlevel[cluster], VNICluster[cluster], VNBCluster[cluster], \
                VNIICluster[cluster], SumIntro))
    print (Outline)
sys.exit(0)
