#!/usr/bin/env python
import sys
import re

if len(sys.argv) < 2:
    raise ValueError("Usage:%s <Gene_Fasta> <H99_BlastParser> <JEC21_BlastParser> <R265_BlastParser> <Treme_BlastParser>" %(sys.argv[0]))

FastaFile = sys.argv[1]
fh_fastafile = open(FastaFile,'r')
SeqNames = {}

for line in fh_fastafile:
    if(line.startswith('>')):
        m = re.findall(r'^>(\S+)',line)
        SeqNames[m[0]] = 0
        
fh_fastafile.close()


StrainFiles = {}
for i in range(2,len(sys.argv)):
    filename = sys.argv[i].split('.')
    species = filename[2]
    StrainFiles[species] = sys.argv[i]

BlastResult = {}
SPECIES = sorted(StrainFiles.keys())


for species in StrainFiles:
    fh_blastparser = open(StrainFiles[species],'r')
    for line in fh_blastparser:
        infors = line.split('\t')
        identity = infors[9]
        coverage = infors[10]
        SeqNames[infors[0]] = 1
        res = {'identity':identity,'coverage':coverage}
        geneblast = {}
        if infors[0] in BlastResult.keys():
            geneblast = BlastResult[infors[0]]
            geneblast[species] = res
        else:
            geneblast[species] = res
            BlastResult[infors[0]] = geneblast
    fh_blastparser.close()

Outline = "Gene_ID"
for species in SPECIES:
    Outline += "\t" + species
print (Outline)

for gene in BlastResult:
    outputline = gene
    geneblast = BlastResult[gene]
    for species in SPECIES:
        if(species in geneblast.keys()):
            res = geneblast[species]
            outputline += "\t" + res['identity'] + "," + res['coverage']
        else:
            outputline += "\t" 
    print (outputline)
    
for gene in SeqNames:
    if(SeqNames[gene] == 0):
        outputline =  gene
        outputline += '\t' * len(SPECIES)
        print (outputline)
