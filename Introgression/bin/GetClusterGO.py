#!/usr/bin/env python

import sys
import csv
import GOEnrich

TarStrain = 'CNA2'
TarCol = 5

clusterID = []
VNICluster = []
VNIICluster = []
VNBCluster = []
geneGOs = {}
clusterGene = {}
H99GO = {}
H99Genes = []

if(len(sys.argv) < 2):
    print ("Usage: <InputFile> <OrthologFile> <GOTable> <H99GeneIDs>")
    sys.exit(1)

Infile = sys.argv[1]
Orthfile = sys.argv[2]
GOFile = sys.argv[3]
H99Genefile = sys.argv[4]
#------------
#input file
#------------
fh_InFile = open(Infile, 'r')
csv_InFile = csv.reader(fh_InFile, delimiter = '\t')
next(csv_InFile)

for line in csv_InFile:
    clusterID.append(line[0])
    if(int(line[3]) > 0):
        VNICluster.append(line[0])
    if(int(line[4]) > 0):
        VNBCluster.append(line[0])
    if(int(line[5]) > 0):
        VNIICluster.append(line[0])
fh_InFile.close()

#--------------
#H99 gene ids
#--------------
fh_H99gfile = open(H99Genefile, 'r')
for line in fh_H99gfile:
    stline = line.strip()
    H99Genes.append(stline)
fh_H99gfile.close()

#------------
#GO File (Only H99)
#------------
fh_GOFile = open(GOFile, 'r')
csv_GOFile = csv.reader(fh_GOFile, delimiter = '\t')
for line in csv_GOFile:
    geneGOs[line[0]] = line[1]
fh_GOFile.close()

#------------
#Ortholog File
#------------
fh_OrthFile = open(Orthfile, 'r')
csv_OrthFile = csv.reader(fh_OrthFile, delimiter = '\t')
lastClusterID = ''
seletedGene = ''
isGO = False


for line in csv_OrthFile:
    if(len(line) < 2):  #skip the empty line
        continue
    if(line[0] not in clusterID): #skip the irrelevant clusters
        continue
    if(line[1] != "CNA2"):
        continue

    if(line[0] not in clusterGene):
        clusterGene[line[0]] = line[5]
    else:
        if(clusterGene[line[0]] not in geneGOs and line[5] in geneGOs):
            clusterGene[line[0]] = line[5]

fh_OrthFile.close()

VNIGenes = []
VNIIGenes = []
VNBGenes = []
FltGenes = []

for cluster in clusterGene:
    FltGenes.append(clusterGene[cluster])
    if(cluster in VNICluster):
        VNIGenes.append(clusterGene[cluster])
    if(cluster in VNIICluster):
        VNIIGenes.append(clusterGene[cluster])
    if(cluster in VNBCluster):
        VNBGenes.append(clusterGene[cluster])

outall = Infile + ".all"
GOEnrich.goenrich(FltGenes, H99Genes, GOFile, outall)
outVNI = Infile + ".vni"
GOEnrich.goenrich(VNIGenes, H99Genes, GOFile, outVNI)
outVNII = Infile + ".vn2"
GOEnrich.goenrich(VNIIGenes, H99Genes, GOFile, outVNII)
outVNB = Infile + ".vnb"
GOEnrich.goenrich(VNBGenes, H99Genes, GOFile, outVNB)


sys.exit(0)
