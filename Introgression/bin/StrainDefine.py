#!/usr/bin/env python
import sys
import matplotlib
matplotlib.use('Agg')
import matplotlib.pyplot as plt
import csv
import numpy
from os import path
import BlastNTCheck as blchk

def column(matrix, i):
    return [row[i] for row in matrix]

def medianvalue(matrix):
    cols = len(matrix[0])
    medians = []
    for i in range(1,cols):
        median = numpy.median(column(matrix,i))
        medians.append(median)
    sortmedian = sorted(medians)
    return sortmedian[0]
    

Iden_Cut = 96
Cov_Cut = 60
Pre_Strain = 'H99'
Gene_File = 'genes.fa'
E_Value = 1e-9

if len(sys.argv) < 3:
    raise ValueError("Usage:%s <Merged_File> <Output_File>" %(sys.argv[0]))

MergeFile = sys.argv[1]
OutputFile = sys.argv[2]
OutImage = OutputFile + ".png"
OutFolder = path.dirname(MergeFile)
FastaFile = OutFolder + '/' + Gene_File

identity = []
coverage = []

blsarray = list(csv.reader(open(MergeFile,'r'),delimiter='\t'))
blsarray_rows = len(blsarray)
blsarray_cols = len(blsarray[0])

Species = blsarray[0][1:]

for row in range(1,blsarray_rows):
    iden_values = []
    cov_values = []
    iden_values.append(blsarray[row][0])
    cov_values.append(blsarray[row][0])
    for col in range(1,blsarray_cols):
        blsres = blsarray[row][col]
        if(blsres == ''):
            iden_values.append(numpy.nan)
            cov_values.append(numpy.nan)
        else:
            blsres_scores = blsres.split(',')
            if(float(blsres_scores[1]) < Cov_Cut):
                iden_values.append(numpy.nan)
                cov_values.append(numpy.nan)
            else:
                iden_values.append(float(blsres_scores[0]))
                cov_values.append(float(blsres_scores[1]))
                
    identity.append(iden_values)
    coverage.append(cov_values)

Iden_Cut2 = medianvalue(identity)

StrainDefine = {}
StrainIden = {}

for i in range(0,len(identity)):
    geneID = identity[i][0]
    
    highest_strain = ""
    highest_iden = 0
    highest_cov = 0
    highnum = 0
    
    for j in range(1,blsarray_cols):
        strain = Species[j-1]

        if(identity[i][j] >= highest_iden):
            highest_strain = strain
            highest_iden = identity[i][j]
            highest_cov = coverage[i][j]
        if(identity[i][j] >= Iden_Cut):
            highnum += 1
    
    IsOtherHighIden = False
    if(highnum > 1):
        IsOtherHighIden = True
    
    StrainDefine[geneID] = "?"
        
        
    if(highest_iden > Iden_Cut):
        if(highest_strain == Pre_Strain):
            if(IsOtherHighIden):
                StrainDefine[geneID] = "*"
            else:
                StrainDefine[geneID] = "-"
        else:
            if(IsOtherHighIden):
                StrainDefine[geneID] = highest_strain + "*"
            else:
                StrainDefine[geneID] = highest_strain
    else:
        if(highest_iden < Iden_Cut2):
            StrainDefine[geneID] = "?"
        else:
            if(highest_strain == Pre_Strain):
                StrainDefine[geneID] = "@"
            else:
                StrainDefine[geneID] = highest_strain + "@"
                
    if(StrainDefine[geneID] == '?'):
        seqrecord = blchk.SearchSeq(geneID,FastaFile)
        bls_res = blchk.BlastSearch(seqrecord,E_Value)
        if(len(bls_res) > 0):
            bls_info = bls_res[0]
            highest_score = highest_iden * highest_cov
            bls_score = bls_info['identity'] * bls_info['coverage']
            if(bls_info['identity'] >= highest_iden and bls_score > highest_score and bls_info['coverage'] >= Cov_Cut):
                organism = blchk.SearchGIOrganism(bls_info['sbjct'])
                if(organism == ""):
                    organism = bls_info['sbjct']
                if(bls_info['identity'] >= Iden_Cut):
                    StrainDefine[geneID] = organism
                elif(bls_info['identity'] >= Iden_Cut2):
                    StrainDefine[geneID] = organism + "@"
                else:
                    StrainDefine[geneID] = organism + "%"
            else:
                if(highest_strain == ""):
                    StrainDefine[geneID] = '?'
                else:
                    if(highest_strain == Pre_Strain):
                        StrainDefine[geneID] = "%"
                    else:
                        StrainDefine[geneID] = highest_strain + "%"

identity_trans = [[r[col] for r in identity] for col in range(1,len(identity[0]))]

plt.figure(1)
plt.boxplot(identity_trans)
plt.xticks(range(1,len(Species)+1),Species)
plt.title("Distribution of mapping identities")
plt.savefig(OutImage)
    
fh_fileout = open(OutputFile,'w')
for geneID in StrainDefine:
    outline = geneID + '\t' + StrainDefine[geneID] + '\n'
    fh_fileout.write (outline)

fh_fileout.close()


