#!/usr/bin/env python
import csv
import sys

CRYPTOS = ["JEC21","WM276","R265"]
SIMBOLS = ["@","?","%"]
SCORES = {'HIGH':1,'REF':2,'CRYPTO':3,'Other':4}

IntroAll = sys.argv[1]
IntroFlt = sys.argv[2]

IntroCluster = []
fh_introflt = open(IntroFlt, 'r')
csv_flt = csv.reader(fh_introflt, delimiter = '\t')
for line in csv_flt:
    IntroCluster.append(line[0])
fh_introflt.close()

fh_introall = open(IntroAll, 'r')
csv_all = csv.reader(fh_introall, delimiter = '\t')
header = next(csv_all)

heads = []
for label in header:
    name = label.replace("Cryp_neof_","")
    name = name.replace("grubii_","")
    name = name.replace("_V1","")
    heads.append(name)
print (",".join(heads))

for line in csv_all:
    if(line[0] in IntroCluster):
        introscores = []
        for col in range(2, len(line)):
            speinfo = line[col].split(':')
            introScore = 1
            for info in speinfo:
                if(info == "-" or info[-1] == "*" or info == "x"):
                    continue
                else:
                    spe = ""
                    if(info[-1] in SIMBOLS):
                        if(info[-1] == "?"):
                            spe = "Others"
                            if(introScore < 4):
                                introScore = 4
                        elif(info[-1] == "@"):
                            if(introScore < 3):
                                introScore = 3
                        elif(info[-1] == "%"):
                            if(introScore < 4):
                                introScore = 4
                    else:
                        if(info in CRYPTOS):
                            if(introScore < 2):
                                introScore = 2
                        else:
                            if(introScore < 4):
                                introScore = 4
            introscores.append(str(introScore))
        print (line[0] + "," + line[1] + "," + ",".join(introscores))

fh_introall.close()
sys.exit(0)
