#!/usr/bin/env python

import sys
import csv

if(len(sys.argv) < 2):
    print ("Usage: <InputFile> <OrthologFile> <AnnotationFile>")
    sys.exit(1)

Infile = sys.argv[1]
Orthfile = sys.argv[2]
Annofile = sys.argv[3]

clusterID = []
#------------
#input file
#------------
fh_InFile = open(Infile, 'r')
csv_InFile = csv.reader(fh_InFile, delimiter = '\t')
next(csv_InFile)

for line in csv_InFile:
    if(line[1] == "1"):
        clusterID.append(line[0])
fh_InFile.close()

#------------
#Ortholog File
#------------
fh_OrthFile = open(Orthfile, 'r')
csv_OrthFile = csv.reader(fh_OrthFile, delimiter = '\t')
H99GeneID = {}

for line in csv_OrthFile:
    if(len(line) < 2):  #skip the empty line
        continue
    if(line[0] not in clusterID): #skip the irrelevant clusters
        continue
    if(line[1] != "CNA2"):
        continue

    H99GeneID[line[5]] = line[0]

fh_OrthFile.close()

fh_annofile = open(Annofile, 'r')
csv_annofile = csv.reader(fh_annofile, delimiter = '\t')

for line in csv_annofile:
    if(len(line) < 2):
        continue
    if(line[2] != 'gene'):
        continue
    gId = line[8][3:13]
    if(gId in H99GeneID):
        print ("%s\t%s\t%s\t%s\t%s" %(H99GeneID[gId], gId, line[0], line[3], line[4]))

fh_annofile.close()
sys.exit(0)
