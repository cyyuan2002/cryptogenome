#!/usr/bin/env python

import sys
import csv

STRAINS = ["cng10", "MW_RSA852", "C45", "cng9", "Bt85", "Tu401_1", "Bt63", \
           "Ze90_1", "cng11", "Bt206", "Bt1", "Bt15", "Bt120", "A1-35-8", "H99", \
           "C23", "AD2_60a", "cng8", "AD1_7a", "D17_1", "A2_102_5", "Tu259_1", \
           "CHC193", "A5-35-17", "MW_RSA1955",  "C8", "AD1_83a", "MW_RSA36", \
           "Gb118", "Br795", "cng7", "cng2", "cng3", "cng6", "cng5", "cng4", \
           "cng1", "Th84", "125_91"]

VNII = ["cng10", "MW_RSA852", "C45", "cng9"]
VNB = ["Bt85", "Tu401_1", "Bt63", "Ze90_1", "cng11", "Bt206", "Bt1"]
VNI = ["Bt15", "Bt120", "A1-35-8", "H99", "C23", "AD2_60a", "cng8", "AD1_7a", \
       "D17_1", "A2_102_5", "Tu259_1", "CHC193", "A5-35-17", "MW_RSA1955", "C8", \
       "AD1_83a", "MW_RSA36", "Gb118", "Br795", "cng7", "cng2", "cng3", \
       "cng6", "cng5", "cng4", "cng1", "Th84", "125_91"]

CRYPTOS = ["JEC21","WM276","R265"]
SIMBOLS = ["@","?","%"]

Infile = sys.argv[1]
fh_file = open(Infile,'r')
reader = csv.reader(fh_file,delimiter = "\t")

header = next(reader)

VNBCluster = {}
VNICluster = {}
VNIICluster = {}
FltCluster = []
Introlevel = {}
H99CopyNum = {}

for line in reader:
    if (int(line[1]) < 1): #skip clusters without H99 ortholog
        continue
    #level1 = 0
    #level2 = 0
    straincount = 0
    H99CopyNum[line[0]] = line[1]
    #VNICluster[line[0]] = 0
    #VNBCluster[line[0]] = 0
    #VNIICluster[line[0]] = 0
    for col in range(2, len(line)):
        speinfo = line[col].split(':')
        strain = header[col]
        strain = strain.replace("Cryp_neof_","")
        strain = strain.replace("grubii_","")
        strain = strain.replace("_V1","")
        level1count = 0
        level2count = 0
        isIntro = False
        for info in speinfo:
            if(info == "-" or info[-1] == "*" or info == "x"):
                continue
            else:
                if(line[0] not in FltCluster):
                    FltCluster.append(line[0])
                spe = ""
                if(info[-1] in SIMBOLS):
                    if(info[-1] == "?"):
                        spe = "Others"
                    elif(info[-1] == "@"):
                        if(len(info) > 1):
                            spe = "Cryptococcus"
                        else:
                            spe = "Cryptococcus"
                    elif(info[-1] == "%"):
                        spe = "Others"
                else:
                    if(info in CRYPTOS):
                        spe = info
                    else:
                        spe = "Others"

                if(spe == ""):
                    print (",".join(line))

                if(strain in VNI):
                    if(spe not in VNICluster):
                        VNICluster[spe] = 1
                    else:
                        VNICluster[spe] += 1
                elif(strain in VNII):
                    if(spe not in VNIICluster):
                        VNIICluster[spe] = 1
                    else:
                        VNIICluster[spe] += 1
                elif(strain in VNB):
                    if(spe not in VNBCluster):
                        VNBCluster[spe] = 1
                    else:
                        VNBCluster[spe] += 1
fh_file.close()

SumVNI = 0
for species in VNICluster:
    SumVNI += VNICluster[species]

for species in VNICluster:
    print ("VNI\t%s\t%s\t%.2f" %(species, VNICluster[species], VNICluster[species]/SumVNI*100))

SumVNB = 0
for species in VNBCluster:
    SumVNB += VNBCluster[species]
for species in VNBCluster:
    print ("VNB\t%s\t%s\t%.2f" %(species, VNBCluster[species], VNBCluster[species]/SumVNB*100))

SumVNII = 0

for species in VNIICluster:
    SumVNII += VNIICluster[species]
for species in VNIICluster:
    print ("VNII\t%s\t%s\t%.2f" %(species, VNIICluster[species], VNIICluster[species]/SumVNII*100))

sys.exit(0)
