#!/usr/bin/env python
import sys
import csv
from Bio import SeqIO
from Bio.Blast import NCBIWWW
from Bio.Blast import NCBIXML
from Bio import Entrez
import re

def __sortHSPs__(Hsps,evalue):
    sorted_hsp = []
    s_index = 0
    while(len(Hsps) > 0):
        for i in range(len(Hsps)):
            if(Hsps[s_index].expect > float(evalue)):
                del Hsps[i]
                break

            if(int(Hsps[i].query_start) < Hsps[s_index].query_start):
                s_index = i
        if(len(Hsps) > 0):
            sorted_hsp.append(Hsps[s_index])
            del Hsps[s_index]
            s_index = 0
    return sorted_hsp

def SearchGIOrganism(seqID):
    Entrez.email = "cyyuan2002@gmail.com"
    seqids = seqID.split('|')
    ncbigi = seqids[1]
    handle = Entrez.efetch(db="nucleotide", id=ncbigi, rettype="gb", retmode="text")
    organism = ""
    for line in handle:
        line = line.strip()
        m = re.match(r'(^ORGANISM\s+)',line)
        if(m):
            end = m.span()[1]
            organism = line[end:]
            break
    return organism

def SearchSeq (seqID,seqFile):
    for record in SeqIO.parse(seqFile,"fasta"):
        if(record.id == seqID):
            return record

def BlastSearch (seqRecord,evalue):
    blast_res = []
    result_handle = NCBIWWW.qblast("blastn","nt",seqRecord.seq)
    blast_records = NCBIXML.parse(result_handle)
    
    for blast_record in blast_records:
        blast_res = __ParserBlastRecord__(blast_record,evalue)
        break
    
    return blast_res

def __ParserBlastRecord__ (blast_record,evalue,onlybst = True):
    align_count = 0
    bls_res = []
    
    for alignment in blast_record.alignments:
        if(onlybst):
            if(align_count > 0):
                break
        align_count += 1
        querynames = blast_record.query.split(' ')
        query_name = querynames[0]
        
        align_res = {}
        
        subname = alignment.title.replace('>','')
        subnames = subname.split(' ')
        sbjct_name = subnames[0]
        query_length = int(blast_record.query_letters)
        sbjct_length = int(alignment.length)
        hsp_info = {}
        sorted_hsps = []
        if(len(alignment.hsps) < 2):
            hsp = alignment.hsps[0]
            if(hsp.expect > float(evalue)):
                next;
            hsp_info['match_length'] = int(hsp.identities)
            hsp_info['align_length'] = int(hsp.align_length)
            hsp_info['query_align_length'] = int(hsp.query_end) - int(hsp.query_start) + 1
            hsp_info['query_start'] = int(hsp.query_start)
            hsp_info['query_end'] = int(hsp.query_end)
            hsp_info['sbjct_start'] = int(hsp.sbjct_start)
            hsp_info['sbjct_end'] = int(hsp.sbjct_end)
            hsp_info['query_alignments'] = str(hsp.query_start)+'-'+str(hsp.query_end)
            hsp_info['sbjct_alignments'] = str(hsp.sbjct_start)+'-'+str(hsp.sbjct_end)
            if(hsp.strand[1] == 'Minus'):
                hsp_info['strand'] = '-'
                hsp_info['sbjct_start'], hsp_info['sbjct_end'] = hsp_info['sbjct_end'], hsp_info['sbjct_start']
            else:
                hsp_info['strand'] = '+'
        else:
            sorted_hsps = __sortHSPs__(alignment.hsps,evalue)
            if(len(sorted_hsps) < 1):
                continue
            hsp_info['query_start'] = int(sorted_hsps[0].query_start)
            hsp_info['query_end'] = int(sorted_hsps[0].query_end)
            hsp_info['sbjct_start'] = int(sorted_hsps[0].sbjct_start)
            hsp_info['sbjct_end'] = int(sorted_hsps[0].sbjct_end)
            hsp_info['query_align_length'] = int(sorted_hsps[0].query_end) - int(sorted_hsps[0].query_start) + 1
            hsp_info['match_length'] = int(sorted_hsps[0].identities)
            hsp_info['align_length'] = int(sorted_hsps[0].align_length)
            hsp_info['query_alignments'] = str(sorted_hsps[0].query_start)+'-'+str(sorted_hsps[0].query_end)
            hsp_info['sbjct_alignments'] = str(sorted_hsps[0].sbjct_start)+'-'+str(sorted_hsps[0].sbjct_end)
            if(sorted_hsps[0].strand[1] == 'Minus'):
                hsp_info['strand'] = '-'
            else:
                hsp_info['strand'] = '+'

            for i in range(1,len(sorted_hsps)):
                hsp_query_length = int(sorted_hsps[i].query_end) - int(sorted_hsps[i].query_start) + 1
                if(sorted_hsps[i].query_start < hsp_info['query_end']):
                    if(sorted_hsps[i].query_end > hsp_info['query_end']):
                        #remove overlapped hsp
                        if((hsp_info['query_end'] - sorted_hsps[i].query_start)/hsp_query_length >= 0.5): 
                            continue
                        hsp_info['query_align_length'] += int(sorted_hsps[i].query_end) - hsp_info['query_end']
                        hsp_info['query_end'] = sorted_hsps[i].query_end
                    else:
                        continue
                else:
                    hsp_info['query_align_length'] += hsp_query_length
                    hsp_info['query_end'] = sorted_hsps[i].query_end
                    
                hsp_info['match_length'] += int(sorted_hsps[0].identities)
                hsp_info['align_length'] += int(sorted_hsps[0].align_length)
                hsp_info['query_alignments'] += ','+str(sorted_hsps[i].query_start)+'-'+str(sorted_hsps[i].query_end)
                hsp_info['sbjct_alignments'] += ','+str(sorted_hsps[i].sbjct_start)+'-'+str(sorted_hsps[i].sbjct_end)
               
                hsp_info['sbjct_end'] = sorted_hsps[i].sbjct_end

        identity = hsp_info['match_length'] / hsp_info['align_length'] * 100
        identity = float("%.2f" % identity)
        alignment_percent = hsp_info['query_align_length'] / query_length * 100
        alignment_percent = float("%.2f" %  alignment_percent)
            
        align_res['query'] = query_name
        align_res['sbjct'] = sbjct_name
        align_res['strand'] = hsp_info['strand']
        align_res['qlength'] = query_length
        align_res['qstart'] = hsp_info['query_start']
        align_res['qend'] = hsp_info['sbjct_end']
        align_res['slength'] = sbjct_length
        align_res['sstart'] = hsp_info['sbjct_start']
        align_res['send'] = hsp_info['sbjct_end']
        align_res['identity'] = identity
        align_res['coverage'] = alignment_percent
        align_res['qaligns'] = hsp_info['query_alignments']
        align_res['saligns'] = hsp_info['sbjct_alignments']
        
        bls_res.append(align_res)
        
    return bls_res
