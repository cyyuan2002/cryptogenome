#!/usr/bin/env python
import sys
import csv
import BlastNTCheck as blchk

E_Value = 1e-9

if(len(sys.argv) < 3):
    print ("Usage:%s <unigene_file> <sequence_folder>" %(sys.argv[0]))
    sys.exit(1)

FileName = sys.argv[1]
SeqFolder = sys.argv[2]
with open(FileName,'r') as unifile:
    filereader = csv.reader(unifile,delimiter = '\t')
    for line in filereader:
        if(len(line)<1): continue
        if(line[1] == "CNA2" or line[1] == "CNB_WM276_v2" or line[1] == "CND1"): continue
        seqID = line[4]
        seqFile = SeqFolder + "/" + line[1] + "/genes.fa"
        #print (line[1] + ":" + line[3])
        seqrecord = blchk.SearchSeq(seqID,seqFile)
        if(seqrecord is None): continue
        bls_res = blchk.BlastSearch(seqrecord,E_Value)
        outline = line[0] + "\t" + line[1] + "\t" + line[3] + "\t" + line[6]
        if(len(bls_res) > 0):
            bls_info = bls_res[0]
            organism = blchk.SearchGIOrganism(bls_info['sbjct'])
            if(organism == ""):
                organism = bls_info['sbjct']
            
            outline += "\t" + organism + "\t" + str(bls_info['identity']) + "\t" + str(bls_info['coverage'])
        else:
            outline += "\tNA\tNA\tNA"
        print(outline)

sys.exit(0)
