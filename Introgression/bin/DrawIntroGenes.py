#!/usr/bin/env python

import sys
import csv
import VisualGenome as vg

if(len(sys.argv) < 2):
    print ("Usage: <IntroTable> <IntroGeneInfo> <OutputFile>")
    sys.exit(1)

IntroTable = sys.argv[1]
GeneInfo = sys.argv[2]
OutputFile = sys.argv[3]

VNIClusters = []
VNIIClusters = []
VNBClusters = []

fh_table = open(IntroTable, 'r')
csv_table = csv.reader(fh_table, delimiter = '\t')
next(csv_table)
for line in csv_table:
    if(line[1] == "1"):
        if(int(line[3]) > 0):
            VNIClusters.append(line[0])
        if(int(line[4]) > 0):
            VNBClusters.append(line[0])
        if(int(line[5]) > 0):
            VNIIClusters.append(line[0])
fh_table.close()

fh_geneinfo = open(GeneInfo, 'r')
csv_geneinfo = csv.reader(fh_geneinfo, delimiter = '\t')
geneRegions = {}

for line in csv_geneinfo:
    regioninfo = vg.RegionInfo(line[2],line[3],line[4])
    geneRegions[line[0]] = regioninfo

fh_geneinfo.close()

H99Genome = vg.CNA2Genome()

VNIRegions = vg.GenomeRegions(H99Genome.chroms)
VNBRegions = vg.GenomeRegions(H99Genome.chroms)
VNIIRegions = vg.GenomeRegions(H99Genome.chroms)
for cluster in VNIClusters:
    VNIRegions.add_region(geneRegions[cluster])

for cluster in VNBClusters:
    VNBRegions.add_region(geneRegions[cluster])

for cluster in VNIIClusters:
    VNIIRegions.add_region(geneRegions[cluster])

regionSets = vg.GenomeRegionSets(H99Genome.chroms)
regionSets.add_regionset(VNIRegions)
regionSets.add_regionset(VNBRegions)
regionSets.add_regionset(VNIIRegions)
vsGenome = vg.VisualGenome()
vsGenome.set_regions(regionSets)
vsGenome.set_centromere(H99Genome.centromeres)
vsGenome.set_auto_chrom_color(True)
vsGenome.set_auto_region_color(True)
vsGenome.set_output_file(OutputFile)
vsGenome.draw_regions()

sys.exit(0)
