#!/usr/bin/env python
"""This script is used to select one gene, which has
GO information, from Ortholog cluster """
#pylint: disable-msg=C0103

import sys
import csv

TarStrain = 'CNA2'
TarCol = 5

geneIds = {}
geneGOs = {}
clusterGene = {}
H99GO = {}

if(len(sys.argv) < 3):
    print ("Usage:%s <Input_File> <Ortholog_File> <GO_File>" %(sys.argv[0]))
    sys.exit(1)

InFile = sys.argv[1]
OrthFile = sys.argv[2]
GOFile = sys.argv[3]

#------------
#input file
#------------
fh_InFile = open(InFile, 'r')
csv_InFile = csv.reader(fh_InFile, delimiter = '\t')
header = next(csv_InFile)

for line in csv_InFile:
    stdNum = int(line[3])
    if(stdNum + int(line[4]) > 0 ):
        geneIds[line[0]] = 'CNA2'
    else:
        for i in range(7, 45):
            if(stdNum + int(line[i]) > 0):
                geneIds[line[0]] = header[i]
                break
fh_InFile.close()


#------------
#GO File (Only H99)
#------------
fh_GOFile = open(GOFile, 'r')
csv_GOFile = csv.reader(fh_GOFile, delimiter = '\t')
for line in csv_GOFile:
    geneGOs[line[0]] = line[1]
fh_GOFile.close()

#------------
#Ortholog File
#------------
fh_OrthFile = open(OrthFile, 'r')
csv_OrthFile = csv.reader(fh_OrthFile, delimiter = '\t')
lastClusterID = ''
seletedGene = ''
isGO = False

for line in csv_OrthFile:
    if(len(line) < 2):  #skip the empty line
        continue
    if(line[0] not in geneIds): #skip the irrelevant clusters
        continue
    else:
        strain = geneIds[line[0]]
        if (line[1] != strain):  #skip other strains
            continue

        if(strain == 'CNA2'):
            if(line[0] not in clusterGene):
                clusterGene[line[0]] = line[TarCol]
                if(line[TarCol] in geneGOs):
                    H99GO[line[TarCol]] = geneGOs[line[TarCol]]
            else:
                if(line[TarCol] in geneGOs and
                   clusterGene[line[0]] not in geneGOs):
                    clusterGene[line[0]] = line[TarCol]
                    H99GO[line[TarCol]] = geneGOs[line[TarCol]]
        else:
            clusterGene[line[0]] = line[3]

#-------------
#Output
#-------------

for clusterID in geneIds:
    Outline = clusterID + "\t" + clusterGene[clusterID] + "\t" + geneIds[clusterID]
    if(geneIds[clusterID] == 'CNA2'):
        if(clusterGene[clusterID] in H99GO):
            Outline += "\t" + H99GO[clusterGene[clusterID]]
    print (Outline)

sys.exit(0)
