#!/usr/bin/env python
"""This script is used to extract fasta sequences
from strain protein sequences for blast2go"""
#pylint: disable-msg=C0103

import sys
import csv
from Bio import SeqIO
from os import path

def GrepFasSeq(seqID, seqFile):
    """get sequences from fasfile"""
    if(path.isfile(seqFile)):
        fh_FasFile = open(seqFile, 'rU')
        for seq in SeqIO.parse(fh_FasFile,"fasta"):
            if(seq.id == seqID):
                print (seq.format('fasta'))
                return 1
    else:
        raise IOError ("Cannot open file: " + seqFile)
    return 0

if(len(sys.argv) < 2):
    print ("Usage:%s <Input_File> <Genome_Folder> " %(sys.argv[0]))
    sys.exit(1)

GenoFolder = sys.argv[2]
InFile = sys.argv[1]
fh_InFile = open(InFile, 'r')
reader = csv.reader(fh_InFile, delimiter = '\t')

for line in reader:
    if(len(line) > 3):
        continue
    else:
        if(line[2] == 'CNA2'):
            continue
        else:
            strain = line[2]
            geneID = line[1]
            strainfolder = GenoFolder + '/' + strain
            pepfile = strainfolder + '/' + strain + \
                      "_FINAL_CALLGENES_1.annotation.pep"
            if(strain == 'Cryp_neof_grubii_A1-35-8_V1' or \
               strain == 'Cryp_neof_grubii_A5-35-17_V1' or \
               strain == 'Cryp_neof_grubii_cng2_V1'):
                pepfile = strainfolder + '/' + strain + \
                          "_FINAL_CALLGENES_2.annotation.pep"
            isGetFile = GrepFasSeq(geneID, pepfile)
            if(not isGetFile):
                sys.stderr.write ("Cannot find seq: %s from strain %s" \
                                  %(geneID, strain))

fh_InFile.close()
sys.exit(0)
