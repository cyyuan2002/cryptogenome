#!/usr/bin/env python

import itertools
import csv
import sys
import re
from matplotlib.backends.backend_pdf import PdfPages
import matplotlib.pyplot as plt
import numpy as np
import random
import copy

STRAINS = ["cng10", "MW_RSA852", "C45", "cng9", "Bt85", "Tu401_1", "Bt63", \
           "Ze90_1", "cng11", "Bt206", "Bt1", "Bt15", "Bt120", "A1-35-8", "CNA2", \
           "C23", "AD2_60a", "cng8", "AD1_7a", "D17_1", "A2_102_5", "Tu259_1", \
           "CHC193", "A5-35-17", "MW_RSA1955",  "C8", "AD1_83a", "MW_RSA36", \
           "Gb118", "Br795", "cng7", "cng2", "cng3", "cng6", "cng5", "cng4", \
           "cng1", "Th84", "125_91"]

VNII = ["cng10", "MW_RSA852", "C45"]
VNB = ["Bt85", "Tu401_1", "Bt63", "Ze90_1", "cng11", "Bt206", "Bt1"]
VNI = ["Bt15", "Bt120", "A1-35-8", "CNA2", "C23", "AD2_60a", "cng8", "AD1_7a", \
       "D17_1", "A2_102_5", "Tu259_1", "CHC193", "A5-35-17", "MW_RSA1955", "C8", \
       "AD1_83a", "MW_RSA36", "Gb118", "Br795", "cng7", "cng2", "cng3", \
       "cng6", "cng5", "cng4", "cng1", "Th84", "125_91"]

def pureName(strName):
    strName = strName.replace("Cryp_neof_","")
    strName = strName.replace("grubii_","")
    strName = strName.replace("_V1","")
    return strName

def choose(n, k):
    if 0 <= k <= n:
        ntok = 1
        ktok = 1
        for t in range(1, min(k, n - k) + 1):
            ntok *= n
            ktok *= t
            n -= 1
        return ntok // ktok
    else:
        return 0

def randcombination(elements,k,number):
    samples = []
    for i in range(number):
        sample = randcomb(copy.copy(elements), k)
        samples.append(sample)
    return samples

def randcomb(elements, k):
    if(k == 1):
        rand_index = random.randrange(len(elements))
        return elements[rand_index]
    else:
        rand_index = random.randrange(len(elements))
        element = []
        m = elements.pop(rand_index)
        element.append(m)
        a = randcomb(elements, k-1)
        if isinstance(a, str):
            element.append(a)
        else:
            element.extend(a)
        return element

def calgenome(strainList, maxpermu = 1000):

    PanCounts = []
    CoreCounts = []
    Panaves = []
    Coreaves = []
    PanStd = []
    CoreStd = []
    counts = []
    for strain in strainList:
        genecount = sum(ClusterCount[strain]) + UniqueCount[strain]
        counts.append(genecount)
    countarray = np.array(counts)
    countave = np.mean(countarray)
    countstd = np.std(countarray)
    Panaves.append(countave)
    Coreaves.append(countave)
    PanCounts.append(counts)
    CoreCounts.append(counts)
    PanStd.append(countstd)
    CoreStd.append(countstd)
    print ("I = %s, combination = %s, Pan Ave = %s, Core Ave = %s"
               %(1, 1, countave, countave))
    for i in range(2, len(strainList)+1):
        pancounts = []
        corecounts = []
        counts = 0
        isselected = False
        combcount = choose(len(strainList), i)
        samples = []
        if(combcount > maxpermu):
            samples = randcombination(strainList, i, maxpermu)
        else:
            samples = [list(x) for x in itertools.combinations(strainList, i)]
        for strains in samples:
            countmatrix = [ClusterCount[strain] for strain in strains]
            uniquenums = [UniqueCount[strain] for strain in strains]
            countarray = np.array(countmatrix)
            count_max = countarray.max(0)
            count_min = countarray.min(0)
            uniqsum = sum(uniquenums)
            count_max_sum = np.sum(count_max)
            count_min_sum = np.sum(count_min)
            pancount = count_max_sum + uniqsum
            pancounts.append(pancount)
            corecounts.append(count_min_sum)
        panarray = np.array(pancounts)
        panave = np.mean(panarray)
        panstd = np.std(panarray)
        corearray = np.array(corecounts)
        coreave = np.mean(corearray)
        corestd = np.std(corearray)
        PanCounts.append(pancounts)
        CoreCounts.append(corecounts)
        Panaves.append(panave)
        Coreaves.append(coreave)
        PanStd.append(panstd)
        CoreStd.append(corestd)
        print ("I = %s, combination = %s, Pan Ave = %s, Core Ave = %s"
               %(i, combcount, panave, coreave))
    ResAll = {'PanCounts': PanCounts, 'CoreCounts': CoreCounts, 'PanAves': Panaves,
              'CoreAves': Coreaves, 'PanStds': PanStd, 'CoreStds': CoreStd}
    return ResAll

TableFile = sys.argv[1] #Crypto38_ORTHOMCLBLASTFILE_4.cluster_dist_per_genome.txt
UniqueFile = sys.argv[2]  #Crypto38_ORTHOMCLBLASTFILE_4.unique

UniqueCount = {}
ClusterCount = {}

fh_unique = open(UniqueFile,'r')
reader_uni = csv.reader(fh_unique, delimiter = '\t')
for line in reader_uni:
    if(len(line) < 1): continue
    strain = pureName(line[1])
    if(strain not in UniqueCount):
        UniqueCount[strain] = 1
    else:
        UniqueCount[strain] += 1
fh_unique.close()

fh_table = open(TableFile,'r')
reader_tbl = csv.reader(fh_table, delimiter = '\t')
next(reader_tbl)
header = next(reader_tbl)
for i in range(2,len(header)):
    strain = pureName(header[i])
    header[i] = strain

rowcount = 0
for line in reader_tbl:
    if(re.search("uniq", line[0])):
        break
    rowcount += 1
    for i in range(2, len(line)):
        if(header[i] not in ClusterCount):
            clustercount = []
            clustercount.append(int(line[i]))
            ClusterCount[header[i]] = clustercount
        else:
            clustercount = ClusterCount[header[i]]
            clustercount.append(int(line[i]))
            ClusterCount[header[i]] = clustercount
fh_table.close()

#-------------
#Figure for all
#-------------
#print ("All Strains:")
#StrainAll_Res = calgenome(STRAINS,5000)
#StrainAll_Pans = StrainAll_Res['PanCounts']
#StrainAll_Cores = StrainAll_Res['CoreCounts']
#avepans = []
#avecores = []
#for i in range(len(StrainAll_Pans)):
#    x = i + 1
#    pancounts = StrainAll_Pans[i]
#    corecounts = StrainAll_Cores[i]
#    plotnum = len(pancounts)
#    plot, = plt.plot([x] * plotnum, pancounts, color = "lightgrey",
#        markeredgecolor = 'none', marker = 'o', linestyle = 'None')
#    plt.plot([x] * plotnum, corecounts, color = "lightgrey",
#        markeredgecolor = 'none', marker = 'o', linestyle = 'None')
#x = list(range(1,len(STRAINS)+1))
#StrainAll_Pan, = plt.plot(x,StrainAll_Res['PanAves'],color = "green",
#                         markeredgecolor = 'none', marker = 's', ms = 5)
#StrainAll_Core, = plt.plot(x,StrainAll_Res['CoreAves'],color = "red",
#                          markeredgecolor = 'none', marker = '^', ms = 5)
#plt.xlim(0, len(STRAINS)+1)
#plt.xlabel('Number of Genomes')
#plt.ylabel('Number of Genes')
#plt.legend([StrainAll_Pan,StrainAll_Core, plot], ["Pan Genome","Core Genome",
#            "Pairwise Comparison"],shadow=True,prop={'size':10},loc = 2)
##plt.show()
#pp = PdfPages('Coregenome.5000.pdf')
#pp.savefig()
#plt.close()
#pp.close()

#-----------------
#Figure for subpopulation
#-----------------
#print ("VNI Strains:")
#VNI_Res = calgenome(VNI, 5000)
#print ("VNB Strains:")
#VNB_Res = calgenome(VNB)
#print ("VNII Strains:")
#VNII_Res = calgenome(VNII)
#
#VNI_x = list(range(1,len(VNI)+1))
#VNI_Core = plt.errorbar(VNI_x, VNI_Res['CoreAves'], yerr=VNI_Res['CoreStds'], marker = 's', ms = 5, color = "blue", markeredgecolor = 'none')
#VNI_Pan = plt.errorbar(VNI_x, VNI_Res['PanAves'], yerr=VNI_Res['PanStds'], marker = '^', ms = 5, color = "blue", markeredgecolor = 'none')
#
#VNB_x = list(range(1,len(VNB)+1))
#VNB_Core = plt.errorbar(VNB_x, VNB_Res['CoreAves'], yerr=VNB_Res['CoreStds'], marker = 's', ms = 5, color = "green", markeredgecolor = 'none')
#VNB_Pan = plt.errorbar(VNB_x, VNB_Res['PanAves'], yerr=VNB_Res['PanStds'], marker = '^', ms = 5, color = "green", markeredgecolor = 'none')
#
#VNII_x = list(range(1,len(VNII)+1))
#VNII_Core = plt.errorbar(VNII_x, VNII_Res['CoreAves'], yerr=VNII_Res['CoreStds'], marker = 's', ms = 5, color = "red", markeredgecolor = 'none')
#VNII_Pan = plt.errorbar(VNII_x, VNII_Res['PanAves'], yerr=VNII_Res['PanStds'], marker = '^', ms = 5, color = "red", markeredgecolor = 'none')
#
#plt.xlim(0, len(VNI)+1)
#plt.ylim(6000,9000)
#plt.xlabel('Number of Genomes')
#plt.ylabel('Number of Genes')
##plt.show()
#plt.plot([1], label = "VNI Strains", color = "blue")
#plt.plot([1], label = "VNB Strains", color = "green")
#plt.plot([1], label = "VNII Strains", color = "red")
#plt.plot([1], label = "Pan Genomes", marker = '^', color = "grey",
#    markeredgecolor = 'none')
#plt.plot([1], label = "Core Genomes", marker = "s", color = "grey",
#    markeredgecolor = 'none')
#plt.legend(loc = 2, shadow = True)
##plt.legend([VNI_Pan, VNI_Core, VNB_Pan, VNB_Core, VNII_Pan, VNB_Pan],
##    ["VNI Pan Genome", "VNI COG", "VNB PAG", "VNB COG", "VNII PAG", "VNII COG"],
##    loc = 2, shadow=True, prop = {'size':8})
#plt.legend(loc = 2, prop = {'size':10}, shadow = True)
#pp = PdfPages('CoreGenomes.Subpop.5000.pdf')
#pp.savefig()
#plt.close()
#pp.close()
#
#sys.exit()


##Figure to merge both###
print ("All Strains:")
StrainAll_Res = calgenome(STRAINS,3000)
#StrainAll_Pans = StrainAll_Res['PanCounts']
#StrainAll_Cores = StrainAll_Res['CoreCounts']
avepans = []
avecores = []
#for i in range(len(StrainAll_Pans)):
#    x = i + 1
#    pancounts = StrainAll_Pans[i]
#    corecounts = StrainAll_Cores[i]
#    plotnum = len(pancounts)
#    plot, = plt.plot([x] * plotnum, pancounts, color = "lightgrey",
#        markeredgecolor = 'none', marker = 'o', linestyle = 'None')
#    plt.plot([x] * plotnum, corecounts, color = "lightgrey",
#        markeredgecolor = 'none', marker = 'o', linestyle = 'None')
x = list(range(1,len(STRAINS)+1))
StrainAll_Pan = plt.errorbar(x,StrainAll_Res['PanAves'],yerr=StrainAll_Res['PanStds'],color = "black",
                         markeredgecolor = 'none', marker = '^', ms = 5)
StrainAll_Core = plt.errorbar(x,StrainAll_Res['CoreAves'],yerr=StrainAll_Res['CoreStds'],color = "black",
                          markeredgecolor = 'none', marker = 's', ms = 5)

print ("VNI Strains:")
VNI_Res = calgenome(VNI, 3000)
print ("VNB Strains:")
VNB_Res = calgenome(VNB)
print ("VNII Strains:")
VNII_Res = calgenome(VNII)
VNI_x = list(range(1,len(VNI)+1))
VNI_Core = plt.errorbar(VNI_x, VNI_Res['CoreAves'], yerr=VNI_Res['CoreStds'], marker = 's', ms = 5, color = "blue", markeredgecolor = 'none')
VNI_Pan = plt.errorbar(VNI_x, VNI_Res['PanAves'], yerr=VNI_Res['PanStds'], marker = '^', ms = 5, color = "blue", markeredgecolor = 'none')

VNB_x = list(range(1,len(VNB)+1))
VNB_Core = plt.errorbar(VNB_x, VNB_Res['CoreAves'], yerr=VNB_Res['CoreStds'], marker = 's', ms = 5, color = "green", markeredgecolor = 'none')
VNB_Pan = plt.errorbar(VNB_x, VNB_Res['PanAves'], yerr=VNB_Res['PanStds'], marker = '^', ms = 5, color = "green", markeredgecolor = 'none')

VNII_x = list(range(1,len(VNII)+1))
VNII_Core = plt.errorbar(VNII_x, VNII_Res['CoreAves'], yerr=VNII_Res['CoreStds'], marker = 's', ms = 5, color = "red", markeredgecolor = 'none')
VNII_Pan = plt.errorbar(VNII_x, VNII_Res['PanAves'], yerr=VNII_Res['PanStds'], marker = '^', ms = 5, color = "red", markeredgecolor = 'none')


plt.ylim(5500,10000)
plt.plot([1], label = "All strains", color = "black")
plt.plot([1], label = "VNI strains", color = "blue")
plt.plot([1], label = "VNB strains", color = "green")
plt.plot([1], label = "VNII strains", color = "red")
plt.plot([1], label = "Pan genomes", marker = '^', color = "grey",
    markeredgecolor = 'none')
plt.plot([1], label = "Core genomes", marker = "s", color = "grey",
    markeredgecolor = 'none')
plt.legend(loc = 2, prop = {'size':9}, shadow = True)
#plt.legend([VNI_Pan, VNI_Core, VNB_Pan, VNB_Core, VNII_Pan, VNB_Pan],
#    ["VNI Pan Genome", "VNI COG", "VNB PAG", "VNB COG", "VNII PAG", "VNII COG"],
#    loc = 2, shadow=True, prop = {'size':8})
#plt.show()
pp = PdfPages('MergeAll.5000.pdf')
pp.savefig()
plt.close()
pp.close()

