#!/usr/bin/env python
"""This script is used to filter GO:19XXXX id in ID2GO file"""

import sys
import csv

Infile = sys.argv[1]
fh_file = open(Infile,"r")
reader = csv.reader(fh_file,delimiter = "\t")
for line in reader:
    GOIDs = line[1].split(", ")
    filtered = []
    for ID in GOIDs:
        idnum = int(ID[3:])
        if(idnum > 1000000):
            continue
        filtered.append(ID)
    if(len(filtered) > 0):
        print (line[0] + "\t" + ", ".join(filtered))

fh_file.close()
sys.exit(0)
