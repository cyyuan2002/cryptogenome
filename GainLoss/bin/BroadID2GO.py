#!/usr/bin/env python
"""This script is used to convert Broad GO tables to ID2GO,
which is required by topGO in R"""
#pylint: disable-msg=C0103
import sys
import csv

if(len(sys.argv)<2):
    print ("Usage:%s <Broad_GOFile>" %(sys.argv[0]))
    sys.exit(1)

GOFile = sys.argv[1]
fh_GOFile = open(GOFile, 'r')
reader = csv.reader(fh_GOFile, delimiter = '\t')
GeneGO = {}
for line in reader:
    locusinfo = line[0]
    locusinfos = locusinfo.split(' ')
    locus = locusinfos[2]
    locusID = locus[6:]
    if(locusID in GeneGO):
        GeneGO[locusID] += ", " + line[1]
    else:
        GeneGO[locusID] = line[1]

fh_GOFile.close()

for gID in GeneGO:
    print (gID + "\t" + GeneGO[gID])

sys.exit(0)
