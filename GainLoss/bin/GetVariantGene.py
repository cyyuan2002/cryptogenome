#!/usr/bin/env python
"""Script used to find gain/loss clusters"""
#pylint: disable-msg=C0103
import csv
import sys

SelGenes = []
DupGenes = []
DelGenes = []
BothGenes = []

SelCols = list(range(7, 45))
SelCols.insert(0, 4)

FileIn = sys.argv[1]
Fh_file = open(FileIn, 'r')
Reader = csv.reader(Fh_file, delimiter='\t')
header = next(Reader)
for line in Reader:
    stats = 0   # 0 no change, 1 dup, 2 del, 3 both
    if(line[0].count('uniq') > 0):
        break
    if (line[2] == 'x'):
        SelGenes.append(line)
        BothGenes.append(line)
    else:
        for i in SelCols:
            if(line[i] != '0'):
                SelGenes.append(line)
                break

        for i in SelCols:
            num = int(line[i])
            if(num < 0):
                if(stats == 0):
                    stats = 2
                elif(stats == 1):
                    stats = 3
            elif(num > 0):
                if(stats == 0):
                    stats = 1
                elif(stats == 2):
                    stats = 3
        if(stats == 1):
            DupGenes.append(line)
        elif(stats == 2):
            DelGenes.append(line)
        elif(stats == 3):
            BothGenes.append(line)
Fh_file.close()

OutFileAll = FileIn + ".flt"
OutFileDup = OutFileAll + ".gain"
OutFileDel = OutFileAll + ".loss"
OutFileBoth = OutFileAll + ".both"

Fh_OutFileAll = open(OutFileAll,'w')
Fh_OutFileAll.write ("\t".join(header)+"\n")
for line in SelGenes:
    Fh_OutFileAll.write("\t".join(line)+"\n")
Fh_OutFileAll.close()

Fh_OutFileDup = open(OutFileDup,'w')
Fh_OutFileDup.write ("\t".join(header)+"\n")
for line in DupGenes:
    Fh_OutFileDup.write("\t".join(line)+"\n")
Fh_OutFileDup.close()

Fh_OutFileDel = open(OutFileDel,'w')
Fh_OutFileDel.write ("\t".join(header)+"\n")
for line in DelGenes:
    Fh_OutFileDel.write("\t".join(line)+"\n")
Fh_OutFileDel.close()

Fh_OutFileBoth = open(OutFileBoth,'w')
Fh_OutFileBoth.write ("\t".join(header)+"\n")
for line in BothGenes:
    Fh_OutFileBoth.write("\t".join(line)+"\n")
Fh_OutFileBoth.close()
