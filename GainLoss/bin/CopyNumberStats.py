#!/usr/bin/env python
#pylint: disable-msg=C0103
import csv
import sys

if(len(sys.argv) < 2):
    print ("Usage:%s <Ortholog_Table>" %(sys.argv[0]))
    sys.exit(1)

OrthFile = sys.argv[1]
with open(OrthFile,'r') as csvFile:
    reader = csv.reader(csvFile, delimiter='\t')
    next(reader, None)
    header = next(reader)
    strainNum = len(header) - 2
    fieldsNum = len(header)
    header.insert(2,"stats")
    header.insert(3,"StdNum")
    print ("\t".join(header))
    for line in reader:
        clsID = line[0]
        des = line[1]
        cpyNums = {}
        outline = clsID + "\t" + des
        cpyNums[line[2]] = 1
        for i in range(5, fieldsNum):
            if(line[i] not in cpyNums):
                cpyNums[line[i]] = 1
            else:
                cpyNums[line[i]] += 1

        stdNum = None
        isSig = False
        for cpynum in cpyNums:
            if(cpyNums[cpynum] > strainNum/2):
                stdNum = cpynum
                isSig = True
                break
        if(stdNum is None):
            maxNum = 0
            for cpynum in cpyNums:
                if(cpyNums[cpynum] > maxNum):
                    stdNum = cpynum
                    maxNum = cpyNums[cpynum]
        if(isSig):
            outline += "\t*\t" + stdNum
        else:
            outline += "\tx\t" + stdNum
        for i in range(2, fieldsNum):
            cpychange = int(line[i]) - int(stdNum)
            outline += "\t" + str(cpychange)
        print (outline)

sys.exit(0)
