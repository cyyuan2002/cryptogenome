#!/usr/bin/env python
"""This script is used to merge H99GO with blast2go results"""
#pylint: disable-msg=C0103
import sys
import csv


if(len(sys.argv) < 3):
    print ("Usage: %s <H99GOFile> <ID2GOFile>" %(sys.argv[0]))
    sys.exit(1)

H99GOFile = sys.argv[1]
InputGOFile = sys.argv[2]

NewGOAnnot = {}
GOAnnot = {}

fh_InFile = open(InputGOFile, 'r')
reader = csv.reader(fh_InFile, delimiter = '\t')

for line in reader:
    NewGOAnnot[line[0]] = line[1]
fh_InFile.close()


fh_H99 = open(H99GOFile, 'r')
reader2 = csv.reader(fh_H99, delimiter = '\t')

for line in reader2:
    if(line[1] in NewGOAnnot):
        line.append(NewGOAnnot[line[1]])
    print ("\t".join(line))
fh_H99.close()

sys.exit(0)
