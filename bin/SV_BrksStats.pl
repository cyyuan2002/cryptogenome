#!/usr/bin/perl
use strict;
use FindBin;
use lib ("$FindBin::Bin");
use Strains;

#my $FLANKING = 50;
##change to 200 for CTX
my $FLANKING = 200;
my $CHROMHEAD = "supercont2.";
my $CHROMNUM = 14;
my @CLADES=("VNI","VNII","VNB");
my %SVAll;
my ($FileAppend,$OutFile,$IsCTX);

sub CheckSV{
    my ($currSV)=@_;
    my %svinfo=%{$currSV};
    my $chrA=$svinfo{'chrA'};
    if (!exists($SVAll{$chrA})) {
        my @ChrSVs;
        push(@ChrSVs,$currSV);
        $SVAll{$chrA}=\@ChrSVs;
    }
    else{
        my @ChrSVs=@{$SVAll{$chrA}};
        my $isfound=0;
        for(my $i=0;$i<@ChrSVs;$i++){
            my %sv=%{$ChrSVs[$i]};
            if ($sv{'chrB'} eq $svinfo{'chrB'}) {
                if ($sv{'posA'} >= $svinfo{'posA'} - $FLANKING && $sv{'posA'} <= $svinfo{'posA'} + $FLANKING) {
                    if ($sv{'posB'} >= $svinfo{'posB'} - $FLANKING && $sv{'posB'} <= $svinfo{'posB'} + $FLANKING) {
                        $sv{'count'}++;
                        $sv{'strain'} = $sv{'strain'}.",".$svinfo{'strain'};
                        if ($IsCTX) {
                            $sv{'dir'} = $sv{'dir'}.",".$svinfo{'dir'};
                        }

                        $ChrSVs[$i]=\%sv;
                        $SVAll{$chrA}=\@ChrSVs;
                        $isfound=1;
                        last;
                    }
                }

            }

        }
        if ($isfound==0) {
            my @ChrSVs=@{$SVAll{$chrA}};
            push(@ChrSVs,$currSV);
            $SVAll{$chrA}=\@ChrSVs;
        }
    }
}

sub MergeSVs{
    my ($refFile,$refStrain,$clade,$OutFile)=@_;
    my @files=@{$refFile};
    my @strains=@{$refStrain};
       for(my $i=0;$i<@files;$i++){
        my $lastchrom;
        my $svcount=0;
        my $svlength=0;
        my %chrsvcount;
        my %chrsvlength;
        open(my $fh_brkfile,$files[$i]);
        while(<$fh_brkfile>){
            chomp();
            my @lines=split(/\t/,$_);
            if (exists($chrsvcount{$lines[0]})) {
                $chrsvcount{$lines[0]}++;
            }
            else{
                $chrsvcount{$lines[0]}=1;
            }
            if (exists($chrsvcount{$lines[2]})) {
                $chrsvcount{$lines[2]}++;
            }
            else{
                $chrsvcount{$lines[2]}=1;
            }
            my %info;
            $info{'chrA'}=$lines[0];
            $info{'posA'}=$lines[1];
            $info{'chrB'}=$lines[2];
            $info{'posB'}=$lines[3];
            $info{'svtype'}=$lines[4];
            $info{'strain'}=$strains[$i];
            if ($IsCTX) {
                $info{'dir'}=$lines[5];
            }

            $info{'count'}=1;
            &CheckSV(\%info);
        }
    }

    open(my $fh_out,">$clade.tmp");
    foreach my $chrom(sort keys %SVAll){
        my @brks=@{$SVAll{$chrom}};
        foreach my $sv(@brks){
            my %svinfo=%{$sv};
            print $fh_out "$svinfo{'chrA'}\t$svinfo{'posA'}\t$svinfo{'chrB'}\t$svinfo{'posB'}\t$svinfo{'svtype'}\t$svinfo{'count'}\t$svinfo{'strain'}";
            if ($IsCTX) {
                print $fh_out "\t$svinfo{'dir'}";
            }
            print $fh_out "\n";
        }
    }
    `cat $clade.tmp | sort -k 1,1 -k 2,2n -k 3,3 -k 4,4n > $OutFile.$clade.sv.rgn`;
    unlink "$clade.tmp";
    %SVAll=();
}

($FileAppend,$OutFile,$IsCTX)=@ARGV;
if(@ARGV<2){
    die "<File_Appendix> <Output_file> <IsCTX:default 0>";
}

$IsCTX ||= 0;
my %brks;

foreach my $clade (@CLADES){
    my @files=GetCladeFiles($clade,$FileAppend);
    my @strains=GetStrainNames($clade);
    MergeSVs(\@files,\@strains,$clade,$OutFile);
}

my @FileAll=GetStrainFiles($FileAppend);
my @StrainAll=GetStrainNames;
MergeSVs(\@FileAll,\@StrainAll,"All",$OutFile);

exit(0);

