#!/usr/bin/env python
import csv
import sys
import copy

if len(sys.argv) < 4:
    sys.stderr.write ("Usage:%s <SnpMatrix> <FilteredLoci> <OutputFile>\n" %(sys.argv[0]))
    sys.exit(1)
    
fmatrix = sys.argv[1]


filterloci = {}
lastchrom = ""
tmploci = []

fh_loci = open(sys.argv[2], 'rU')
loci_reader = csv.reader(fh_loci, delimiter = "\t")

for line in loci_reader:
    if line[0] != lastchrom:
        if len(tmploci) > 0:
            filterloci[lastchrom] = copy.copy(tmploci)
        tmploci = []
        lastchrom = line[0]
    tmploci.append(line[1])
filterloci[lastchrom] = copy.copy(tmploci)
fh_loci.close()

fh_matrix = open(sys.argv[1], 'rU')
fh_out = open(sys.argv[3], 'w')

header = fh_matrix.next()
fh_out.write(header)

for line in fh_matrix:
    info = line.split("\t")
    if info[0] != lastchrom:
        if info[0] not in filterloci:
            raise ValueError ("line error: %s\ncannot find chrom %s" %(line, info[0]))
        lastchrom = info[0]
        tmploci = filterloci[info[0]]
    if info[1] in tmploci:
        continue
    fh_out.write(line)

fh_matrix.close()
fh_out.close()