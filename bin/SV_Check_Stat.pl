#!/usr/bin/perl
use strict;
use FindBin;
use lib ("$FindBin::Bin");
use Strains;

my $app="chk";
my @Files=GetStrainFiles($app);
my @Strains=GetStrainNames();
my %statstotal=('Varified'=>0,'Transloc'=>0,'Failed'=>0);
for(my $i=0;$i<@Files;$i++){
    open(my $fh_filein,"$Files[$i]");
    my $total;
    my %stats=('Varified'=>0,'Transloc'=>0,'Failed'=>0);
    while (<$fh_filein>) {
        chomp();
        $total++;
        my @lines=split(/\t/,$_);
        $stats{$lines[0]}++;
        $statstotal{$lines[0]}++;
    }
    close $fh_filein;
    print "$Strains[$i]\t$Files[$i]\t$total";
    foreach my $key(keys %stats){
        print "\t$key\t$stats{$key}";
    }
    print "\n";
}

print "Total\t";
foreach my $key(keys %statstotal){
    print "\t$key\t$statstotal{$key}";
}
print "\n";