#!/usr/bin/env python
import csv
import sys

if __name__ == '__main__':
    if len(sys.argv) < 4:
        sys.stderr.write ("Usage:%s <SNP_Matrix> <Columns of Group1:4,5> <Columns of Group2:6,7>\n" %(sys.argv[0]))
        sys.exit(1)
    
    fmatrix = sys.argv[1]
    group1 = sys.argv[2].split(',')
    group2 = sys.argv[3].split(',')
    
    group1 = map(int, group1)
    group2 = map(int, group2)
    
    fh_matrix = open(fmatrix, 'rU')
    reader = csv.reader(fh_matrix, delimiter='\t')
    reader.next()
    print ("#CHROM\tPOS\tREF\tALT\tGroup1\tGroup2")
    for line in reader:
        algroup1 = []
        algroup2 = []
        for col in group1:
            if line[col] != '.' and line[col] not in algroup1:
                algroup1.append(line[col])
        
        for col in group2:
            if line[col] != '.' and line[col] not in algroup2:
                algroup2.append(line[col])
        
        if len(algroup1) > 1 or len(algroup2) > 1:
            continue
        
        if algroup1[0] != algroup2[0]:
            print ("%s\t%s\t%s" %("\t".join(line[0:4]),algroup1[0],algroup2[0]))
