#!/usr/bin/perl
use strict;
use lib '/Users/Alvin/Workspace/Dropbox/Crypto/38Strains/bin';
use SVRegions;


my ($RepOutMerge,$SVFile) = @ARGV;
my %RepRegion = %{SVRegions::ReadRegionFiles($RepOutMerge,4,5,6,1,3,1,1)};
open(my $fh_sv,"$SVFile") or die "Can't open file: $SVFile\n";
while (my $line=<$fh_sv>) {
    chomp($line);
    my @lines = split(/\t/,$line);
    my $chrom = $lines[0];
    my $posA = $lines[1];
    my $posB = $lines[3];
    my $SVlength = $posB-$posA+1;
    my $overlength = 0;
    my @Regions = @{$RepRegion{$chrom}};
    for(my $i=0;$i<@Regions;$i++){
        my $ta=$Regions[$i]->{'s'};
        my $tb=$Regions[$i]->{'e'};
        if ($posA <= $ta && $posB >= $ta) {
            if ($posB>=$tb) {
                $overlength += $tb - $ta +1;
            }
            else{
                $overlength += $posB - $ta +1;
            }
        }
        elsif($posA <= $tb && $posB >= $tb){
            if ($posA < $ta) {
                $overlength += $tb - $ta;
            }
            else{
                $overlength += $tb - $posA;
            }
        }
        elsif($posA >= $ta && $posB <= $tb){
            $overlength += $posB - $posA +1;
        }
        elsif($ta >= $posA && $tb <= $posB){
            $overlength += $tb - $ta +1;
        }
        if ($overlength / $SVlength >= 0.8) {
            print "$line\n";
            last;
            
        }
        last if ($ta > $posB);
    }
    
}
close $fh_sv;

exit(0);

