#!/usr/bin/perl
use strict;

my ($file,$straincount)=@ARGV;
die "Usage:$0 <Clade_File> <Clade_strain_num>\n" if(@ARGV<2);
open(my $fh_in,"<","$file");
my $outfile="$file.cvt2";
open(my $fh_out,">","$outfile");
while (<$fh_in>) {
    chomp();
    my @lines=split(/\t/,$_);
    my $chrom=$lines[0];
    $chrom=~s/supercont2./chr/g;
    my $percent=$lines[5]/$straincount;
    print $fh_out "$chrom\t$lines[1]\t$lines[3]\t$percent\n";
}
close $fh_in;
close $fh_out;

