#!/usr/bin/perl
use strict;

#!/usr/bin/perl
use strict;

sub ObtainDelgene{
    my @Strainfiles=@_;
    my $Outfile=shift(@Strainfiles);
    my %RepeatSeq;
    for(my $i=0;$i<@Strainfiles;$i++){
        open(my $fh_filein,"$Strainfiles[$i]");
        while (<$fh_filein>) {
            chomp();
            my @lines=split(/\t/,$_);
            if ($lines[5] =~ /R=\S+/) {
                $lines[10] =~ /CDS_Len:(\d+)/;
                my $CDSlength = $1;
                $lines[8] =~ /MID:(\S+)/;
                my @genes = split(",",$1);
                if ($CDSlength > 300) {
                    foreach my $gene(@genes){
                        if (!exists($RepeatSeq{$lines[5]})) {
                            $RepeatSeq{$lines[5]} = \@genes;
                        }
                        else{
                            my @temparray=@{$RepeatSeq{$lines[5]}};
                            push(@temparray,@genes);
                            $RepeatSeq{$lines[5]} = \@temparray;
                        }
                    }
                }
            }
        }
    }
    open(my $fh_out,">$Outfile\n");
    foreach my $gene(keys %RepeatSeq){
        my @genes=@{$RepeatSeq{$gene}};
        my @unigene=keys %{{map{$_=>1}@genes}};
        print $fh_out $gene,"\t",join(",",@unigene),"\n";
        #print $fh_out $gene,"\t",join(",",@genes),"\n";
    }
    close $fh_out;
}

my ($FileName) = shift;
if ($FileName eq "") {
    die "Usage:$0 <File_Name>\n";
}

my $OutFile="$FileName.glist";
ObtainDelgene($OutFile,$FileName);

exit(0);