package Strains;
use strict;
use Exporter;


our @ISA=qw(Exporter);
our @EXPORT=qw(GetStrainFiles GetCladeFiles GetStrainNames GetStrainFiles2);

our @STRAINS=("cng10","MW_RSA852","C45","cng9","Bt85","Tu401_1","Bt63","Ze90_1",
              "cng11","Bt206","Bt1","Bt15","Bt120","A1","H99","C23","AD2_60a",
              "cng8","AD1_7a","D17_1","A2_102_5","Tu259_1","CHC193","A5","MW_RSA1955",
              "C8","AD1_83a","MW_RSA36","Gb118","Br795","cng7","cng2","cng3","cng6",
              "cng5","cng4","cng1","Th84","125.91");

our @VNII=("cng10","MW_RSA852","C45");
our @VNB=("Bt85","Tu401_1","Bt63","Ze90_1","cng11","Bt206","Bt1");
##remove H99 from the list
our @VNI=("Bt15","Bt120","A1","C23","AD2_60a",
              "cng8","AD1_7a","D17_1","A2_102_5","Tu259_1","CHC193","A5","MW_RSA1955",
              "C8","AD1_83a","MW_RSA36","Gb118","Br795","cng7","cng2","cng3","cng6",
              "cng5","cng4","cng1","Th84","125.91");

our %CLADES=('VNI'=>\@VNI,'VNII'=>\@VNII,'VNB'=>\@VNB);

sub GetStrainFiles{
    my ($Appendix,$Dir)=@_;
    $Dir ||= ".";
    opendir(my $dr_DIR,$Dir);
    my @files=readdir $dr_DIR;
    my @filteredfiles = grep (/\.$Appendix$/,@files);
    my @orderedfiles;
    for (my $i=0;$i<@STRAINS;$i++){
        my $isfound=0;
        foreach my $file(@filteredfiles){
            my $fileN=TrimFile($file);
            if ($fileN=~/$STRAINS[$i]\./) {
                push(@orderedfiles,$file);
                $isfound=1;
                last;
            }
        }
        if ($isfound==0) {
            print STDERR "WARNING: Cannot find file for strain $STRAINS[$i]!!\n";
        }
    }
    return @orderedfiles;
}

sub GetStrainNames{
    my $strainN = shift;
    $strainN ||= "All";
    if ($strainN eq "All") {
        return @STRAINS;
    }
    elsif($strainN eq "VNI"){
        return @VNI;
    }
    elsif($strainN eq "VNII"){
        return @VNII;
    }
    elsif($strainN eq "VNB"){
        return @VNB;
    }
    else{
        print STDERR "WARNING: Cannot find strain clade $strainN!!\n";
        return 0;
    }
}

sub GetCladeFiles{
    my ($Clade,$Appendix,$Dir)=@_;
    $Dir ||= ".";
    if (!exists($CLADES{$Clade})) {
        die "ERROR: Cannot find clade $Clade!!\n";
    }
    
    opendir(my $dr_DIR,$Dir);
    my @strains=@{$CLADES{$Clade}};
    my @files=readdir $dr_DIR;
    my @filteredfiles = grep (/\.$Appendix$/,@files);
    my @orderedfiles;
    for (my $i=0;$i<@strains;$i++){
        my $isfound=0;
        my $strain = $strains[$i];
        foreach my $file(@filteredfiles){
            my $fileN=TrimFile($file);
            if ($fileN=~/$strain\./) {
                push(@orderedfiles,$file);
                $isfound=1;
                last;
            }
        }
        if ($isfound==0) {
            print STDERR "WARNING: Cannot find file for strain $strains[$i]!!\n";
        }
    }
    return @orderedfiles;
}

sub TrimFile{
    my $filename=shift;
    my @File=split(/\./,$filename);
    shift @File;
    my $newname=join(".",@File);
    return $newname;
}

sub GetStrainFiles2{
    my ($Appendix,$Dir)=@_;
    $Dir ||= ".";
    opendir(my $dr_DIR,$Dir);
    my @files=readdir $dr_DIR;
    my @filteredfiles = grep (/\.$Appendix$/,@files);
    my %strainfiles;
    for (my $i=0;$i<@STRAINS;$i++){
        my $isfound=0;
        foreach my $file(@filteredfiles){
            my $fileN=TrimFile($file);
            if ($fileN=~/$STRAINS[$i]\./) {
                $strainfiles{$STRAINS[$i]} = $file;
                $isfound=1;
                last;
            }
        }
        if ($isfound==0) {
            print STDERR "WARNING: Cannot find file for strain $STRAINS[$i]!!\n";
        }
    }
    return \%strainfiles;
}
