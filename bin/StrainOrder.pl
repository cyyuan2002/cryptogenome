#!/usr/bin/perl
use strict;
use Cwd;
my $path=getcwd;

my @StrainOrder=("rcng10","rMW_RSA852","rC45","rcng9","rBt85","rTu401_1","rBt63","rZe90_1",
                 "rcng11","rBt206","rBt1","rBt15","rBt120","rA1","rH99","rC23","rAD2_60a",
                 "rcng8","rAD1_7a","rD17_1","rA2_102_5","rTu259_1","rCHC193","rA5",
                 "rMW_RSA1955","rC8","rAD1_83a","rMW_RSA36","rGb118","rBr795","rcng7",
                 "rcng2","rcng3","rcng6","rcng5","rcng4","rcng1","rTh84","r125");

my @filename;
my $count=1;
while (<>) {
    chomp();
    push(@filename,$_);
}

foreach my $strainN (@StrainOrder){
    foreach my $fileN(@filename){
        my @fileInfo=split(/\./,$fileN);
        if ($fileInfo[1] eq $strainN) {
            print "$path/$fileN\n";
            last;
        }
    }
}

exit(0);