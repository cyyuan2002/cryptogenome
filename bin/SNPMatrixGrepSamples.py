#!/usr/bin/env python
import csv
import sys
from math import floor

def findcols (header, strains):
    straincols = []
    
    for strain in strains:
        if strain not in header:
            sys.stderr.write("Error:cannot find %s in header!\n" %(strain))
            sys.exit(1)
        else:
            straincols.append(header.index(strain))
    
    return straincols

if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.stderr.write ("Usage:%s <matrix_file> <strain_file> [missing call filter: default 0.2]\n" %(sys.argv[0]))
        sys.exit(1)
    
    fmatrix = sys.argv[1]
    fstrain = sys.argv[2]
    ncratio = 0.2
    NonChar = '.'
    
    
    if len(sys.argv) == 4:
        ncratio = sys.argv[3]
    
    fh_strain = open(fstrain, 'rU')
    strains = [x.strip('\n') for x in fh_strain.readlines()]
    fh_strain.close()
    
    fh_matrix = open(fmatrix, 'rU')
    reader = csv.reader(fh_matrix, delimiter="\t")
    header = reader.next()
    ncnum = ncratio * len(strains)
    straincols = findcols(header, strains)
    print ("#CHROM\tPOS\tREF\tALT\t"+"\t".join(strains))
    for line in reader:
        alcounts = {}
        for col in straincols:
            if(line[col] not in alcounts):
                alcounts[line[col]] = 1
            else:
                alcounts[line[col]] += 1
        if len(alcounts) < 2:
            continue
        if len(alcounts) == 2:
            if NonChar in alcounts:
                continue
        if NonChar in alcounts:
            if(alcounts[NonChar] > ncnum):
                continue
        outline = "\t".join(line[0:4])
        for col in straincols:
            outline = outline + "\t" + line[col]
        print outline
