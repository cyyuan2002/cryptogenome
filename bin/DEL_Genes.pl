#!/usr/bin/perl
use strict;
use FindBin;
use lib ("$FindBin::Bin");
use Strains;
my @CLADES=("VNI","VNII","VNB");

sub ObtainDelgene{
    my @Strainfiles=@_;
    my $Outfile=shift(@Strainfiles);
    my %Delgenes;
    for(my $i=0;$i<@Strainfiles;$i++){
        open(my $fh_filein,"$Strainfiles[$i]");
        while (<$fh_filein>) {
            chomp();
            my @lines=split(/\t/,$_);
            $lines[11] =~ /CDS_Len:(\d+)/;
            my $CDSlength = $1;
            $lines[9] =~ /MID:(\S+)/;
            my @genes = split(",",$1);
            if ($CDSlength > 30) {
                foreach my $gene(@genes){
                    if (!exists($Delgenes{$gene})) {
                        $Delgenes{$gene}=1;
                    }
                }
            }
        }
    }
    open(my $fh_out,">$Outfile\n");
    foreach my $gene(keys %Delgenes){
        print $fh_out "$gene\n";
    }
    close $fh_out;
}

my ($FileName) = @ARGV;
if (@ARGV < 1) {
    die "Usage:$0 <File_Name>\n";
}

my $OutFile="$FileName.glist";
ObtainDelgene($OutFile,$FileName);

exit(0);