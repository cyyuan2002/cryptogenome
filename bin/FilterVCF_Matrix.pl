#!/usr/bin/perl
use strict;

my ($mtxFile, $vcfFile) = @ARGV;
if (@ARGV < 2) {
    print stderr "Usage:$0 <matrix_File> <VCF_File>\n";
    exit(1)
}

open(my $fh_mtx, "$mtxFile") or die "Cannot open file $mtxFile\n";
my %selectedSNPs;
my $lastChrom;
my @loci;

while (<$fh_mtx>) {
    #next if $. < 2;
    my @lines = split(/\t/,$_);
    if ($lines[0] ne $lastChrom) {
        if ($lastChrom ne "") {
            my @temparray = @loci;
            $selectedSNPs{$lastChrom} = \@temparray;
        }
        @loci = ();
        $lastChrom = $lines[0]
    }
    push(@loci,$lines[1])
}
my @temparray = @loci;
$selectedSNPs{$lastChrom} = \@temparray;
close($fh_mtx);

open(my $fh_vcf,"$vcfFile") or die "Cannot open file $vcfFile\n";
my $currChr = "";
my @currLoci;
my $index = 0;
while (<$fh_vcf>) {
    if (/^#/) {
        print $_;
        next;
    }
    my @lines = split(/\t/,$_);
    if ($currChr ne $lines[0]) {
        if (exists($selectedSNPs{$lines[0]})) {
            @currLoci = @{$selectedSNPs{$lines[0]}};
            $index = 0;
            $currChr = $lines[0];
        }
    }
    if (@currLoci <= $index) {
        next;
    }
    if ($lines[1] == $currLoci[$index]) {
        print $_;
        $index ++;
    }
}
close $fh_vcf;
exit(0)
