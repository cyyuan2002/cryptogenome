#!/usr/bin/perl
use strict;

my %STRAINCLADE=("cng10"=>"VNII","MW_RSA852"=>"VNII","C45"=>"VNII",
                  "Bt85"=>"VNB","Tu401_1"=>"VNB","Bt63"=>"VNB","Ze90_1"=>"VNB",
                  "cng11"=>"VNB","Bt206"=>"VNB","Bt1"=>"VNB","Bt15"=>"VNI",
                  "Bt120"=>"VNI","A1"=>"VNI","H99"=>"VNI","C23"=>"VNI",
                  "AD2_60a"=>"VNI","cng8"=>"VNI","AD1_7a"=>"VNI",
                  "D17_1"=>"VNI","A2_102_5"=>"VNI","Tu259_1"=>"VNI",
                  "CHC193"=>"VNI","A5"=>"VNI","MW_RSA1955"=>"VNI","C8"=>"VNI",
                  "AD1_83a"=>"VNI","MW_RSA36"=>"VNI","Gb118"=>"VNI","Br795"=>"VNI",
                  "cng7"=>"VNI","cng2"=>"VNI","cng3"=>"VNI","cng6"=>"VNI",
                  "cng5"=>"VNI","cng4"=>"VNI","cng1"=>"VNI","Th84"=>"VNI","125.91"=>"VNI");

my $SV_MergedFile=shift;
my %Clade_SV;

open(my $file_in,"$SV_MergedFile") or die "Cannot open file: $SV_MergedFile\n";
while (my $line=<$file_in>) {
    chomp($line);
    my @lines=split(/\t/,$line);
    my @strains=split(/,/,$lines[6]);
    next if($lines[5] < 2);
    my $clade=$STRAINCLADE{$strains[0]};
    my $issame=1;
    for(my $i=1;$i<@strains;$i++){
        next if(!exists($STRAINCLADE{$strains[$i]}));
        if ($STRAINCLADE{$strains[$i]} ne $clade){
            $issame=0;
            last;
        }
    }
    if ($issame==1) {
        if (!exists($Clade_SV{$clade})) {
            my @svs;
            push(@svs,$line);
            $Clade_SV{$clade}=\@svs;
        }
        else{
            my @svs=@{$Clade_SV{$clade}};
            push(@svs,$line);
            $Clade_SV{$clade}=\@svs;
        }
    }
}
close $file_in;

foreach my $clade(keys %Clade_SV){
    my @svs=@{$Clade_SV{$clade}};
    open(my $file_out,">$SV_MergedFile.$clade.uni");
    foreach my $sv(@svs){
        print $file_out "$sv\n";
    }
    close $file_out;
}

exit(0);
