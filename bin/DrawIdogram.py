#!/usr/bin/env python
import cairo
import sys
import copy
import colorsys
import operator

IMG_WIDTH, IMG_LENGTH = 1920,500

F_ChromLength = ""
F_SVMerge = ""
F_OutPDF = ""
GenomeLength = 0
SVLength = 0

if len(sys.argv) < 4:
    raise ValueError("Usage:%s <Chrom_Length> <SV_Merges> <OUT_FILE>" %(sys.argv[0]))
else:
    F_ChromLength = sys.argv[1]
    F_SVMerge = sys.argv[2]
    F_OutPDF = sys.argv[3]
    
Chroms = {}
ChromsOrder = []

Fh_Chrom = open(F_ChromLength,'r')
for line in Fh_Chrom:
    strip = line.strip()
    infor = strip.split('\t')
    Chroms[infor[0]] = int(infor[1])
    GenomeLength += int(infor[1])
    ChromsOrder.append(infor[0])
    
Fh_Chrom.close()

SVregions = {}
Fh_SVMerges = open(F_SVMerge,'r')
lastChrom = ""
SV_Chrom = {}


for line in Fh_SVMerges:
    strip = line.strip()
    infor = strip.split('\t')
    if lastChrom != infor[0]:
        if(lastChrom != ""):
            SVregions[lastChrom] = copy.deepcopy(SV_Chrom)
        SV_Chrom = {}
        lastChrom = infor[0]
    SV_Chrom[int(infor[1])] = int(infor[2])
    SVLength += int(infor[2]) - int(infor[1])
SVregions[lastChrom] = copy.deepcopy(SV_Chrom)
Fh_SVMerges.close()

LengthPix = IMG_WIDTH * 0.9/GenomeLength
SVPix = IMG_WIDTH * 0.9/SVLength
GraphLeft = IMG_WIDTH * 0.05
GraphTop = 50
ChromHeight = 10
Chrom_count = 0
ColorChange = 1/len(ChromsOrder)

surface = cairo.PDFSurface (F_OutPDF, IMG_WIDTH, IMG_LENGTH)
ctx = cairo.Context(surface)
ctx.set_source_rgb(1,1,1)
ctx.rectangle(0,0,IMG_WIDTH,IMG_LENGTH)
ctx.fill()

curpos = GraphLeft
curtop = GraphTop

for chrom in ChromsOrder:
    chrom_color = colorsys.hsv_to_rgb(Chrom_count*ColorChange,1,0.8)
    chrom_width = Chroms[chrom] * LengthPix

    ctx.set_source_rgb(chrom_color[0],chrom_color[1],chrom_color[2])
    ctx.rectangle(curpos,GraphTop,chrom_width,ChromHeight)
    ctx.fill()
    
    ctx.set_source_rgb(0,0,0)
    txt = chrom.replace("supercont2.","chr")
    ctx.set_font_size(12)
    x_off, y_off, tw, th = ctx.text_extents(txt)[:4]
    ctx.move_to(chrom_width/2+curpos-x_off-tw/2,40)
    ctx.show_text(txt)
    curpos += chrom_width
    Chrom_count += 1

contop1 = GraphTop + ChromHeight + 5
contop2 = contop1 + 5
contop3 = contop2 + 20
conbot = contop3 + 5

curlen = 0
curpos = GraphLeft
ctx.new_path()

for chrom in ChromsOrder:
    chrom_sv = SVregions[chrom]
    sorted_sv = sorted(chrom_sv.keys())
    for startpos in sorted_sv:
        endpos = chrom_sv[startpos]
        absstart = curlen + startpos
        absend = curlen + endpos
        startpoint1 = absstart * LengthPix + GraphLeft
        startpoint2 = curpos
        endpoint1 = absend * LengthPix + GraphLeft
        endpoint2 = curpos + (endpos - startpos)*SVPix
        curpos = endpoint2 + SVPix
        
        ctx.move_to(startpoint1,contop1)
        ctx.line_to(startpoint1,contop2)
        ctx.line_to(startpoint2,contop3)
        ctx.line_to(startpoint2,conbot)
        
        ctx.move_to(endpoint1,contop1)
        ctx.line_to(endpoint1,contop2)
        ctx.line_to(endpoint2,contop3)
        ctx.line_to(endpoint2,conbot)
        
    curlen += Chroms[chrom]
    
ctx.set_line_width(0.25)
ctx.stroke()
surface.finish()