#!/usr/bin/env python
'''This script is used to calculate AMOVA and FST for H99 based genome using Alerquine
It requires SNPMatrix and given population information. The format of population
information is as follows:
@pop1
sample1
sample2
sample2
@pop2
sample3
sample4

'''
from __future__ import division
import csv
import sys
import Bio.Seq
import copy
import tempfile
import os.path
import shutil
import subprocess

LIBRARY_PATH = "/Users/Alvin/Dropbox/Python/Library/"
sys.path.append(LIBRARY_PATH)
import Arlequin
import CNA2Genome

ARLERQUNIN = "/Users/Alvin/bin/arlecore_macosx/arlecoremac_64bit"
##missing data in one population
POPMISSING = 0.34

def readPopInfo(popfile):
    fh_pop = open(popfile, 'rU')
    pops = {}
    popname = ''
    popitems = []
    for line in fh_pop:
        lineinfo = line.strip()
        if lineinfo[0] == '@':
            if popname != '':
                pops[popname] = copy.copy(popitems)
            popname = lineinfo[1:]
            popitems = []
        else:
            popitems.append(lineinfo)
    fh_pop.close()
    pops[popname] = copy.copy(popitems)
    return (pops)

def readMatrix(fmatrix, pops, windowsize, tempdir, outfile):
    fh = open(fmatrix, 'rU')
    outmatix = outfile + ".used"
    outres = outfile + ".res"
    fo_matrix = open(outmatix, 'w')
    fo_res = open(outres, 'w')
    
    popNames = sorted(pops.keys()) #sort popnames in order
    reader = csv.reader(fh, delimiter = '\t')
    header = reader.next()
    
    ##write head for output files
    fo_matrix.write("\t".join(header)+"\n")
    reshead = "#chrom\tframecount\tregion\tAmova(among)\tAmova(within)\tFST\tpval"
    for i in xrange(len(popNames)):
        for j in xrange(i+1,len(popNames)):
            reshead += "\tFST(%s:%s)" %(popNames[i],popNames[j])
            reshead += "\tPval(%s:%s)" %(popNames[i],popNames[j])
    fo_res.write(reshead+"\n")
    
    samplecols = findCols(header, pops)
    samplepops = samplecol2samplepop(samplecols)
    framecount = 1
    lastchrom = ""
    matrixrows = []
    siterange = framecount * windowsize
    for line in reader:
        if lastchrom != line[0]:
            if len(matrixrows) > 4:
                arlres = calFST(matrixrows, samplepops, tempdir)
                resout = formatRes(popNames, lastchrom,
                                   framecount, windowsize, arlres)
                #resout = formatRes(popNames, lastchrom,
                #                   framecount, windowsize, None)
                fo_res.write(resout+"\n")
                fo_matrix.write("\n".join(["\t".join(x) for x in matrixrows])+"\n")
            else:
                if lastchrom != "":
                    resout = formatRes(popNames, lastchrom,
                                   framecount, windowsize, None)
                    fo_res.write(resout+"\n")
            framecount = 1
            matrixrows = []
            lastchrom = line[0]
            siterange = framecount * windowsize
        if siterange < int(line[1]):
            if len(matrixrows) > 4:
                arlres = calFST(matrixrows, samplepops, tempdir)
                resout = formatRes(popNames, lastchrom,
                                   framecount, windowsize, arlres)
                #resout = formatRes(popNames, lastchrom,
                #                   framecount, windowsize, None)
                fo_res.write(resout+"\n")
                fo_matrix.write("\n".join(["\t".join(x) for x in matrixrows])+"\n")
            else:
                resout = formatRes(popNames, lastchrom,
                                   framecount, windowsize, None)
                fo_res.write(resout+"\n")
            framecount += 1
            siterange = framecount * windowsize
            matrixrows = []
        if chkMissingCall(line, samplecols):
            #fo_matrix.write("\t".join(line)+"\n")
            matrixrows.append(line)
            #print line
    if len(matrixrows) > 0:
        arlres = calFST(matrixrows, samplepops, tempdir) ##calculate last window
        resout = formatRes(popNames, lastchrom, framecount, windowsize, arlres)
        #resout = formatRes(popNames, lastchrom, framecount, windowsize, None)
        fo_res.write(resout+"\n")
        fo_matrix.write("\n".join(["\t".join(x) for x in matrixrows])+"\n")
    else:
        resout = formatRes(popNames, lastchrom, framecount, windowsize, None)
        fo_res.write(resout+"\n")
    
    fo_matrix.close()
    fo_res.close()

def formatRes(popNames, chrom, framecount, windowsize, arlres):
    windowS =(framecount - 1) * windowsize +  1
    windowE = framecount * windowsize
    outInfo = "%s\t%s\t%s-%s\t" %(chrom,framecount,windowS,windowE)
    if arlres is None:
        outInfo += "NA\tNA\tNA\tNA"
        spaces = "\tNA"*(len(popNames) * (len(popNames)-1))
        outInfo += spaces
    else:
        amova = arlres.getAmova()
        outInfo += "%s\t%s\t%s\t%s" %(amova[0], amova[1], arlres.getFST(), arlres.getFSTPval())
        for i in xrange(len(popNames)):
            for j in xrange(i+1,len(popNames)):
                outInfo += "\t" + arlres.getPWFST(popNames[i], popNames[j])
                outInfo += "\t" + arlres.getPWFSTPval(popNames[i], popNames[j])
    #print outInfo
    return outInfo

def calFST(matrix, samplepops, tempdir):
    ntmatrix = []
    for i in xrange(len(matrix)):
        nts = covGT2NT(matrix[i])
        ntmatrix.append(nts)
    colnum = len(ntmatrix[0])
    rownum = len(ntmatrix)
    seqs = {}
    for i in xrange(rownum):
        for j in xrange(colnum):
            if j not in seqs:
                seqs[j] = ntmatrix[i][j]
            else:
                seqs[j] += ntmatrix[i][j]
    
    arlseq = Arlequin.ArlSeqs("An example of DNA sequence data")
    arlseq.addSeqs(seqs)
    arlseq.addPops(samplepops)
    #print(arlseq.output())
    tempN = ""
    with tempfile.NamedTemporaryFile(dir=tempdir, suffix='.arp') as temp:
        temp.write(arlseq.output())
        temp.flush()
        tempN = temp.name
        subprocess.call ([ARLERQUNIN,"run_silent",temp.name], stdout=open(os.devnull, "w")
            , stderr=subprocess.STDOUT)
    alrresfile = getResfile(tempN)
    alrres = Arlequin.ArlResParser(alrresfile)
    alresfolder = shutil.rmtree(os.path.dirname(alrresfile))
    #amova = alrres.getAmova()
    #print amova
    return alrres
    

def getResfile(tempfile):
    path = os.path.splitext(tempfile)
    basename = os.path.basename(tempfile)
    temp = basename.split('.')
    resfile = path[0]+'.res/' + temp[0] + ".xml"
    return resfile

def samplecol2samplepop(samplecols):
    samplepops = {}
    for pop in samplecols:
        newcols = []
        samples = samplecols[pop]
        for col in samplecols[pop]:
            newcols.append(col-4)
        samplepops[pop] = newcols
    return samplepops

def chkMissingCall(matrixrow,samplecols):
    for pop in samplecols:
        popcols = samplecols[pop]
        miscount = 0
        for col in popcols:
            if matrixrow[col] == '.':
                #miscount += 1
                return False ##remove all loci with missing calls
        misrate = miscount/len(popcols)
        if misrate > POPMISSING:
            return False
    return True

def covGT2NT(matrixrow):
    gts = []
    gts.append(matrixrow[2])
    gts.extend(matrixrow[3].split(','))
    nts = []
    for i in xrange(4,len(matrixrow)):
        if matrixrow[i] == '.':
            nts.append('?')
        else:
            nts.append(gts[int(matrixrow[i])])
    return nts
    
def findCols(header, pops):
    samplecols = {}
    for pop in pops:
        subpop = pops[pop]
        cols = []
        for sample in subpop:
            if sample not in header:
                sys.stderr.write("Error:cannot find %s in header!\n" %(sample))
                sys.exit(1)
            else:
                cols.append(header.index(sample))
        samplecols[pop] = cols
    return samplecols

if __name__ == '__main__':
    if len(sys.argv) < 4:
        sys.stderr.write ("Usage:%s <SNPMatrix> <PopInfo> <TempDir> [WindowSize]" %(sys.argv[0]))
        sys.exit(1)
    
    fmatrix = sys.argv[1]
    fpop = sys.argv[2]
    
    tempdir = sys.argv[3]
    
    windowSize = 5000
        
    if len(sys.argv) == 5:
        windowSize = int(sys.argv[4])
    fout = "%s.%s.%s" %(fmatrix, fpop,windowSize)

    pops = readPopInfo(fpop)
    readMatrix(fmatrix, pops, windowSize, tempdir, fout)