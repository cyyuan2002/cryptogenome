#!/usr/bin/perl
use strict;
use FindBin;
use lib ("$FindBin::Bin");
use Strains;

my ($DELAppend,$DELFolder,$DUPAppend,$DUPFolder) = @ARGV;


my %DELAll = %{GetStrainFiles2($DELAppend,$DELFolder)};
my %DUPAll = %{GetStrainFiles2($DUPAppend,$DUPFolder)};

foreach my $strain (keys %DELAll){
    my $tempfile =  "aCneoH99.r$strain.tmp";
    my $outfile = "aCneoH99.r$strain.cnv.brks";
    my $DELFile = "$DELFolder\/$DELAll{$strain}";
    my $DUPFile = "$DUPFolder\/$DUPAll{$strain}";
    `cat $DELFile $DUPFile > $tempfile`;
    `cat $tempfile | sort -k 1,1 -k 2,2n -k 3,3 -k 4,4n > $outfile`;
}

