#!/usr/bin/perl
use strict;
use FindBin;
use lib ("$FindBin::Bin");
use Strains;

sub MergeSVs{
    my ($refFile,$refStrain,$clade)=@_;
    my @files=@{$refFile};
    my @strains=@{$refStrain};
    my $minSV = 0;
    my $minStrain = "";
    my $maxSV = 0;
    my $maxStrain = "";
    my $totalSVcount = 0;
    for(my $i=0;$i<@files;$i++){
        open(my $fh_brkfile,$files[$i]);
        my @lines = <$fh_brkfile>;
        close $fh_brkfile;
        $totalSVcount += scalar(@lines);
        if ($minStrain eq "") {
            $minStrain = $strains[$i];
            $maxStrain = $strains[$i];
            $minSV = scalar(@lines);
            $maxSV = scalar(@lines);
        }
        else{
            if ($minSV > scalar(@lines)) {
                $minSV = scalar(@lines);
                $minStrain = $strains[$i];
            }
            if ($maxSV < scalar(@lines)) {
                $maxSV = scalar(@lines);
                $maxStrain = $strains[$i];
            }
        }
    }
    print "Population:$clade\n";
    print "Min SV:$minStrain($minSV)\nMax SV:$maxStrain($maxSV)\n";
    my $meanSV = $totalSVcount/scalar(@strains);
    print "Mean SV:$meanSV\n";
}
    
my ($FileAppend)=@ARGV;
if(@ARGV<1){
    die "<File_Appendix>";
}

my %brks;
my @CLADES=("VNI","VNII","VNB");

foreach my $clade (@CLADES){
    my @files=GetCladeFiles($clade,$FileAppend);
    my @strains=GetStrainNames($clade);
    MergeSVs(\@files,\@strains,$clade);
}

exit(0);

