cd Workspace/Repeats/RepeatScout
cp ~/Crypto_NHGRI/assemblies/CneoH99.assembly.fasta .
~/bin/RepeatScout-1/build_lmer_table -l 13 -sequence CneoH99.assembly.fasta -freq tablen.out
~/bin/RepeatScout-1/RepeatScout -l 13 -sequence CneoH99.assembly.fasta -freq tablen.out -output repeatLib.fasta
~/bin/RepeatScout-1/filter-stage-1.prl repeatLib.fasta > repeatLib.flt1.fasta
RepeatMasker -lib repeatLib.flt1.fasta CneoH99.assembly.fasta
cat repeatLib.flt1.fasta | ~/bin/RepeatScout-1/filter-stage-2.prl --cat=CneoH99.assembly.fasta.out > repeatLib.flt2.fasta
RepeatMasker -lib repeatLib.flt2.fasta CneoH99.assembly.fasta
perl ../../../bin/RepeatMerge.pl CneoH99.assembly.fasta.out > CneoH99.assembly.fasta.out.merge
perl ~/Workspace/Dropbox/perl/Anno/Chrom_SilidingWindow_Coverage.pl -i CneoH99.assembly.fasta.out.merge -c Chrom_Length -w 2000 -s 500  > CneoH99.assembly.fasta.out.merge.stats
perl ../../../bin/SNP_stats.pl -i aCneoH99.r39Strains.snp.refHom.noData.matrix.site -c Chrom_Length -w 2000 -s 500 > aCneoH99.r39Strains.snp.refHom.noData.matrix.stats
cat CneoH99.assembly.fasta.out | perl -e '{while(<>){$line=$_;$line=~s/^\s+//;$line=~/\s+$/;@lines=split(/\s+/,$line);print "$lines[4]\t$lines[5]\t$lines[4]\t$lines[6]\tREP\t$lines[9]\n"}}' > CneoH99.assembly.fasta.out.tbl
perl ~/bin/perl/Anno/SV_GeneAnno.pl ../../SV/CNA2_FINAL_CALLGENES_2.trans.gff3 CneoH99.assembly.fasta.out.tbl > CneoH99.assembly.fasta.out.anno