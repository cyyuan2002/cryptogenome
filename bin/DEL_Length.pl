#!/usr/bin/perl
use strict;
use FindBin;
use lib ("$FindBin::Bin");
use Strains;

my @CLADES=("VNI","VNII","VNB");

my ($FileAppend,$Outfile)=@ARGV;
if (@ARGV < 2) {
    die "Usage:$0 <FileAppendix> <OutFile>\n";
}

foreach my $clade (@CLADES){
    my @files=GetCladeFiles($clade,$FileAppend);
    open(my $fh_out,">$Outfile.$clade.len") or die "$!\n";
    foreach my $file(@files){
        open(my $fh_file,$file) or die "Cannot open file $file\n";
        while (<$fh_file>) {
            chomp();
            my @lines=split(/\t/,$_);
            my $length=$lines[3]-$lines[1]+1;
            print $fh_out "$length\n";
        }
        close $fh_file;
    }
    close $fh_out;
}
