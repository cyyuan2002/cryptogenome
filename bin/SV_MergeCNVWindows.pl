#!/usr/bin/perl
use strict;
use FindBin;
use lib ("$FindBin::Bin");
use Strains;


my ($FileAppend) = shift;
die "Usage:$0 <FileAppendix>" if ($FileAppend eq "");

my @Windows;
my @Strains;
my %FileAll=%{GetStrainFiles2($FileAppend)};
my @Colinfor;

my $colcount = 0;
my $isFirst = 1;
foreach my $strain (keys %FileAll){
    my $file = $FileAll{$strain};
    open(my $fh_file,"$file");
    my $rowcount = 0;
    while (<$fh_file>) {
        chomp();
        my @lines = split(/\t/,$_);
        my $value = pop(@lines);
        $Windows[$rowcount][$colcount] = $value;
        $rowcount ++;
        if ($isFirst) {
            push (@Colinfor,join("\t",@lines));
        }
    }
    close $fh_file;
    $colcount++;
    $isFirst = 0;
    push (@Strains, $strain);
}

print "Chrom\tstart\tend\t",join("\t",@Strains),"\n";

for (my $i = 0;$i<@Windows;$i++){
    my @row = @{$Windows[$i]};
    print "$Colinfor[$i]\t";
    print join("\t",@row),"\n";
}
