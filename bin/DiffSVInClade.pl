#!/usr/bin/perl -w

#===============================================================================
#
#         FILE: 
#
#        USAGE:
#
#  DESCRIPTION:
#
#      OPTIONS: ---
# REQUIREMENTS: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Yuan Chen
#      COMPANY: Division of Infectious Disease, DUMC
#      VERSION: 1.0
#      CREATED: 
#     REVISION:
#===============================================================================

use strict;

sub ReadSVFile{
    my ($FileName)=shift;
    open(my $fh_filein, "$FileName") or die "Can't open $FileName\n";
    my @SVs;
    while (<$fh_filein>) {
        chomp();
        my @lines=split(/\t/,$_);
        my %brkinfo;
        $brkinfo{'chrA'}=$lines[0];
        $brkinfo{'chrB'}=$lines[2];
        $brkinfo{'posA'}=$lines[1];
        $brkinfo{'posB'}=$lines[3];
        $brkinfo{'count'}=$lines[5];
        push(@SVs,\%brkinfo);
    }
    close $fh_filein;
    return (@SVs);
}

sub OutputSV{
    my ($refBrks,$OutFile,$minCount,$SV_type)=@_;
    my @Brks=@{$refBrks};
    open(my $fh_out, ">", "$OutFile");
    foreach my $brk(@Brks){
        if ($brk->{'count'} >= $minCount) {
            print $fh_out $brk->{'chrA'},"\t",$brk->{'posA'},"\t",$brk->{'chrB'},
            "\t",$brk->{'posB'},"\t",$SV_type,"\t",$brk->{'count'},"\n";
        }
    }
    close $fh_out;
}

sub CladeCompare{
    my ($refArray1,$refArray2,$Flk_len)=@_;
    my @Array1=@{$refArray1};
    my @Array2=@{$refArray2};
    my %Array1rev;
    my %Array2rev;
    for(my $i=0;$i<@Array1;$i++){
        my %infoA=%{$Array1[$i]};
        for(my $j=0;$j<@Array2;$j++){
            my %infoB=%{$Array2[$j]};
            if ($infoA{'chrA'} eq $infoB{'chrA'} && $infoA{'chrB'} eq $infoB{'chrB'}) {
                if($infoA{'posA'} - $Flk_len <= $infoB{'posA'} && $infoA{'posA'} + $Flk_len >= $infoB{'posA'}){
                    if ($infoA{'posB'} - $Flk_len <= $infoB{'posB'} && $infoA{'posB'} + $Flk_len >= $infoB{'posB'}) { 
                        $Array1rev{$i}=1;
                        $Array2rev{$j}=1;
                    }
                }
            }
        }
    }
    my @newArray1;
    my @newArray2;
    for(my $i=0;$i<@Array1;$i++){
        if (!exists($Array1rev{$i})) {
            push(@newArray1,$Array1[$i]);
        }
    }
    for(my $i=0;$i<@Array2;$i++){
        if (!exists($Array2rev{$i})) {
            push(@newArray2,$Array2[$i]);
        }
    }
    return (\@newArray1,\@newArray2);
}

my ($SV_VN1,$SV_VN2,$SV_VNB,$SV_type,$MinCount,$Flanking) = @ARGV;
if (@ARGV<4) {
    die "Usage: <SV_VN1> <SV_VN2> <SV_VNB> <SV_type> [MinCount:default 2] [Flanking_lenth:default=100]\n";
}

$MinCount ||= 2;
$Flanking ||=100;
my (@VN1brks,@VN2brks,@VNBbrks);

@VN1brks=ReadSVFile($SV_VN1);
@VN2brks=ReadSVFile($SV_VN2);
@VNBbrks=ReadSVFile($SV_VNB);

my ($refVN1brks,$refVN2brks,$refVNBbrks);
($refVN1brks,$refVN2brks)=CladeCompare(\@VN1brks,\@VN2brks,$Flanking);
@VN1brks=@{$refVN1brks};
@VN2brks=@{$refVN2brks};

($refVN1brks,$refVNBbrks)=CladeCompare(\@VN1brks,\@VNBbrks,$Flanking);
@VN1brks=@{$refVN1brks};
@VNBbrks=@{$refVNBbrks};

($refVN2brks,$refVNBbrks)=CladeCompare(\@VN2brks,\@VNBbrks,$Flanking);
@VN2brks=@{$refVN2brks};
@VNBbrks=@{$refVNBbrks};

my $VN1out="$SV_VN1.uni";
my $VN2out="$SV_VN2.uni";
my $VNBout="$SV_VNB.uni";

OutputSV(\@VN1brks,$VN1out,$MinCount,$SV_type);
OutputSV(\@VN2brks,$VN2out,$MinCount,$SV_type);
OutputSV(\@VNBbrks,$VNBout,$MinCount,$SV_type);

exit(0);
