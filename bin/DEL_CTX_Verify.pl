 #!/usr/bin/perl
use strict;
use Getopt::Long;
use File::Temp;
use File::Basename;
use Bio::DB::Fasta;

my ($parRstFile,$parDtlFile,$parGenomeFile,$parOutFile,$parFlankLen,$parSVType,$parMinIdentity,$parMinLengthCov1,$parMinLengthCov2);

sub DelBlastCheck{
    my ($Seq,$Subgenome,$Identitycut,$Lengthcut)=@_;
    my $FasTempfile=File::Temp::tempnam(".","fastmp");
    $FasTempfile=basename($FasTempfile);
    $FasTempfile="$FasTempfile.fas";
    open(my $fh_outfile, ">$FasTempfile");
    print $fh_outfile ">Tempseq\n$Seq\n";
    close $fh_outfile;
    my $blastouts=`blastn -query $FasTempfile -subject $Subgenome -outfmt 6 -evalue 1e-10 -dust no`;
    #print "$blastouts\n";
    unlink $FasTempfile;
    my @blasthits=TileBlast($blastouts);
    my $ispassed=0;
    my $seqlen=length($Seq);
    foreach my $hit(@blasthits){
        if($hit->{'identity'}=$Identitycut && $hit->{'qmatch'}>=($seqlen*$Lengthcut)){
                $ispassed=1;
                last;
        }
    }
    return $ispassed;
}

sub TileBlast{
    my $blastres=shift;
    my @hits=FormatHits($blastres);
    my @treatedhits;
    my @temphits;
    my $lastchr;
    foreach my $hit(@hits){
        my %hitinfo=%{$hit};
        if ($lastchr ne $hitinfo{'sN'}) {
            if ($lastchr ne "") {
                if (@temphits < 2) {
                    push (@treatedhits,@temphits);
                }
                else{
                    my @mergedhits=MergeHits(@temphits);
                    push(@treatedhits,@mergedhits);
                }
            }
            $lastchr = $hitinfo{'sN'};
            @temphits = ();
        }
        push(@temphits,$hit);
    }
    {
        if (@temphits < 2) {
            push (@treatedhits,@temphits);
        }
        else{
            my @mergedhits=MergeHits(@temphits);
            push(@treatedhits,@mergedhits);
        }
    }
    return @treatedhits;
}

sub MergeHits{
    my @hits=SortHits(@_);
    my $MAXGAP=$parFlankLen*0.6;
    my @mergedhits;
    my $ischanged=1;
    while ($ischanged==1) {
        $ischanged=0;
        for(my $i=1;$i<@hits;$i++){
            if ($hits[$i-1]->{'dir'} eq $hits[$i]->{'dir'}  ) {
                if ($hits[$i]->{'sS'} - $hits[$i-1]->{'sE'} < $MAXGAP) {
                    my ($qS,$qE,$sS,$sE,$identity,$qmatch,$smatch);             
                    $identity=($hits[$i]->{'qmatch'}*$hits[$i]->{'identity'}+$hits[$i-1]->{'qmatch'}*$hits[$i-1]->{'identity'})/($hits[$i]->{'qmatch'}+$hits[$i-1]->{'qmatch'});
                    $identity=sprintf("%.2f",$identity);
                    
                    if ($hits[$i]->{'sS'} > $hits[$i-1]->{'sE'}) {
                        $smatch=($hits[$i-1]->{'sE'}-$hits[$i-1]->{'sS'}+1)+($hits[$i]->{'sE'}-$hits[$i]->{'sS'}+1);
                    }
                    else{
                        $smatch=($hits[$i]->{'sE'}-$hits[$i-1]->{'sS'}+1);
                    }
                    $sS=$hits[$i-1]->{'sS'};
                    $sE=$hits[$i]->{'sE'};
                    
                    my $hitA=$hits[$i-1];
                    my $hitB=$hits[$i];
                    if ($hitA->{'qS'} > $hitB->{'qS'}) {
                        my $temphit=$hitA;
                        $hitA=$hitB;
                        $hitB=$temphit;
                    }
                    if ($hitB->{'qS'} > $hitA->{'qE'}) {
                        $qmatch=($hitA->{'sE'}-$hitA->{'sS'}+1)+($hitB->{'sE'}-$hitB->{'sS'}+1);
                    }
                    else{
                        $qmatch=($hitB->{'sE'}-$hitA->{'sS'}+1);
                    }
                    $qS=$hitA->{'qS'};
                    $qE=$hitB->{'qE'};
                    my $mergedhit;
                    $mergedhit->{'qN'}=$hitA->{'qN'};
                    $mergedhit->{'sN'}=$hitA->{'sN'};
                    $mergedhit->{'dir'}=$hitA->{'dir'};
                    $mergedhit->{'qmatch'}=$qmatch;
                    $mergedhit->{'smatch'}=$smatch;
                    $mergedhit->{'identity'}=$identity;
                    splice(@hits,$i-1,2,$mergedhit);
                    $ischanged=1;
                    last;
                }
            }
        }
    }
    return @hits;
}

sub FormatHits{
    my $blastrst=shift;
    my @hits=split(/\n/,$blastrst);
    my @formattedhits;
    foreach my $hit(@hits){
        my @infor=split("\t",$hit);
        my %hitinfo;
        $hitinfo{'qN'}=$infor[0];
        $hitinfo{'sN'}=$infor[1];
        $hitinfo{'identity'}=$infor[2];
        $hitinfo{'qmatch'}=$infor[3];
        $hitinfo{'smatch'}=$infor[3];
        $hitinfo{'qS'}=$infor[6];
        $hitinfo{'qE'}=$infor[7];
        if ($infor[8] < $infor[9]) {
            $hitinfo{'dir'}="+";
            $hitinfo{'sS'}=$infor[8];
            $hitinfo{'sE'}=$infor[9];
        }
        else{
            $hitinfo{'dir'}="-";
            $hitinfo{'sE'}=$infor[8];
            $hitinfo{'sS'}=$infor[9];
        }
        push(@formattedhits,\%hitinfo);
    }
    return @formattedhits;
}

sub SortHits{
    my @hits=@_;
    my @sorted;
    while(scalar(@hits)>0){
	my $minindex;
	my $minstart;
	$minstart=$hits[0]->{'sS'};
        $minindex=0;
	for(my $i=1;$i<@hits;$i++){
	    my $sstart=$hits[$i]->{'sS'};
	    if($sstart<$minstart){
		$minstart=$sstart;
		$minindex=$i;
	    }
	}
	push(@sorted,$hits[$minindex]);
	splice(@hits,$minindex,1);
    }
    return @sorted;
}

sub Revcom{
    my $seq=shift;
    $seq=~tr/atcgATCG/tagcTAGC/;
    $seq=reverse $seq;
    return $seq;
}

sub BreakCheck{
    my ($SVinfo,$Refgenome,$Subgenome,$Flankout)=@_;
    my @svinfos=split(/\t/,$SVinfo);
    my $SVtype=$svinfos[4];
    if($SVtype eq "DEL" ){
        my ($Chr,$Pos1,$Pos2);
        $Chr=$svinfos[0];
        $Pos1=$svinfos[1];
        $Pos2=$svinfos[3];
        my ($pos1s,$pos1e,$pos2s,$pos2e);
        my $chrlength=$Refgenome->length($Chr);
        my $refseq;
        $pos1s=$Pos1-$Flankout+1;
        $pos1s = 1 if($pos1s < 1);
        $pos1e=$Pos1;
        $pos2s=$Pos2;
        $pos2e=$Pos2+$Flankout-1;
        $pos2e = $chrlength if($pos2e > $chrlength);
        my $refregion1="$Chr:$pos1s-$pos1e";
        my $refseq1=$Refgenome->seq($refregion1);
        my $refregion2="$Chr:$pos2s-$pos2e";
        my $refseq2=$Refgenome->seq($refregion2);
        $refseq=$refseq1.$refseq2;
        my ($blastcheck1,$blastcheck2);
        $blastcheck1=DelBlastCheck($refseq,$Subgenome,$parMinIdentity,$parMinLengthCov1);
        if ($blastcheck1==1) {
            my $refregion="$Chr:$Pos1-$Pos2";
            $refseq=$Refgenome->seq($refregion);
            $blastcheck2=DelBlastCheck($refseq,$Subgenome,$parMinIdentity,$parMinLengthCov2);
            if ($blastcheck2==0) {
                return 2;
            }
            else{
                return 1;
            }
        }
        else{
            return 0;
        }
    }
    elsif($SVtype eq "CTX"){
        my @svinfos=split(/\t/,$SVinfo);
        my ($Chr1,$Pos1,$Chr2,$Pos2,$SVtype,@Dirs,$refseq);
        $Chr1=$svinfos[0];
        $Pos1=$svinfos[1];
        $Chr2=$svinfos[2];
        $Pos2=$svinfos[3];
        $SVtype=$svinfos[4];
        my $chrlength1=$Refgenome->length($Chr1);
        my $chrlength2=$Refgenome->length($Chr2);
        
        @Dirs=split("to",$svinfos[5]);
        my ($pos1s,$pos1e,$pos2s,$pos2e);
        if ($Dirs[0] == 5) {
            $pos1s=$Pos1;
            $pos1e=$Pos1+$Flankout-1;
        }
        else{
            $pos1s=$Pos1-$Flankout+1;
            $pos1e=$Pos1;
        }
        $pos1s = 1 if($pos1s < 1);
        $pos1e = $chrlength1 if($pos1e > $chrlength1);
        
        if ($Dirs[1] == 5) {
            $pos2s=$Pos2;
            $pos2e=$Pos2+$Flankout-1;
        }
        else{
            $pos2s=$Pos2-$Flankout+1;
            $pos2e=$Pos2;
        }
        $pos2s = 1 if($pos2s < 1);
        $pos2e = $chrlength2 if($pos2e > $chrlength2);
        
        my $refregion1="$Chr1:$pos1s-$pos1e";
        my $refseq1=$Refgenome->seq($refregion1);
        $refseq1=Revcom($refseq1) if($Dirs[0] == 5);
        my $refregion2="$Chr2:$pos2s-$pos2e";
        my $refseq2=$Refgenome->seq($refregion2);
        $refseq2=Revcom($refseq2) if($Dirs[1] == 3);
        $refseq=$refseq1.$refseq2;
        my $blastcheck=DelBlastCheck($refseq,$Subgenome,$parMinIdentity,0.7);
    }
}

my %opts;
GetOptions(\%opts,"i=s","f:i","t=s","g=s","o:s");
if(! defined($opts{i}) || ! defined($opts{g}) || ! defined($opts{t})){
    die "Usage: <-i sv_result> <-g reference_genome> <-t SV_type>[-f flanking] [-o output_File]\n";
}

$parRstFile=$opts{i};
$parGenomeFile=$opts{g};
$parOutFile=$opts{o};
$parFlankLen=$opts{f};
$parSVType=$opts{t};
$parMinIdentity=90;
$parMinLengthCov1=0.9;
$parMinLengthCov2=0.8;

$parOutFile ||= "$parRstFile.out";
$parFlankLen ||= 250;


if (!-e ($parGenomeFile)) {
    die "Can't open file: $parGenomeFile\n";
}

if (-e ("$parGenomeFile.index")) {
    unlink "$parGenomeFile.index";
}

my $RefGenome=Bio::DB::Fasta->new($parGenomeFile);
$parRstFile=~/aCneoH99.r(\S+)\.\w+.brks/;
my $SubGenome="Cneo\_$1\_V1";

open(my $fh_svfile,"$parRstFile") or die "Can't open file: $parRstFile\n";
while (<$fh_svfile>) {
    chomp();
    my @lines=split(/\t/,$_);
    my $alignrst=BreakCheck($_,$RefGenome,$SubGenome,$parFlankLen);
    if ($parSVType eq "DEL") {
        if ($alignrst==0) {
            print "Failed\t$_\n";
        }
        elsif($alignrst==1){
            print "Transloc\t$_\n";
        }
        elsif($alignrst==2){
            print "Varified\t$_\n";
        }
    }
    if ($parSVType eq "CTX") {
        if ($alignrst == 1) {
            print "Varified\t$_\n";
        }
        else{
            print "Failed\t$_\n";
        }
    }
    
}
close $fh_svfile;