#!/usr/bin/env python
import sys
import copy
import operator

WINDOW_SIZE = 500
MINOVER = 0.2

F_svregion = ""
F_svCalls = ""

if len(sys.argv) < 3:
    raise ValueError("Usage:%s <SV_regions> <SV_Calls>" %(sys.argv[0]))
else:
    F_svregion = sys.argv[1]
    F_svCalls = sys.argv[2]

SV_Calls = {}
Chroms = {}
lastChrom = ""

Fh_svCalls = open(F_svCalls,'r')
for line in Fh_svCalls:
    striped = line.strip()
    infor = striped.split('\t')
    if(lastChrom != infor[0]):
        if(lastChrom != ""):
            SV_Calls[lastChrom] = copy.deepcopy(Chroms)
        Chroms = {}
        lastChrom = infor[0]
        
    startpos = (int(infor[1])//WINDOW_SIZE)*WINDOW_SIZE
    endpos = (int(infor[3])//WINDOW_SIZE+1)*WINDOW_SIZE
    
    if (int(infor[1]) - startpos) >= WINDOW_SIZE * (1-MINOVER):
        startpos = startpos + WINDOW_SIZE
        
    if (int(infor[3]) - endpos) >= WINDOW_SIZE * MINOVER:
        endpos = endpos - 2 *WINDOW_SIZE
    else:
        endpos = endpos - WINDOW_SIZE
        
    for i in range(startpos,endpos,WINDOW_SIZE):
        if(infor[4] == "DUP"):
            Chroms[i] = infor[5]
        else:
            Chroms[i] = 0
            
SV_Calls[lastChrom] = copy.deepcopy(Chroms)

Fh_svregion = open(F_svregion,'r')

Chroms_windows = {}
SV_Values = {}

lastChrom = ""
for line in Fh_svregion:
    striped = line.strip()
    infor = striped.split('\t')
    if(lastChrom != infor[0]):
        if(lastChrom != ""):
            SV_Values[lastChrom] = copy.deepcopy(Chroms_windows)
        Chroms_windows = {}
        lastChrom = infor[0]
        
    startpos = (int(infor[1])//WINDOW_SIZE)*WINDOW_SIZE
    endpos = (int(infor[2])//WINDOW_SIZE+1)*WINDOW_SIZE
    
    if (int(infor[1]) - startpos) >= WINDOW_SIZE * (1-MINOVER):
        startpos = startpos + WINDOW_SIZE
        
    if (int(infor[2]) - endpos) >= WINDOW_SIZE * MINOVER:
        endpos = endpos - 2 *WINDOW_SIZE
    else:
        endpos = endpos - WINDOW_SIZE
    
    if infor[0] not in SV_Calls:
        for i in range(startpos,endpos,WINDOW_SIZE):
            Chroms_windows[i] = 1
    else:
        tempSVs = SV_Calls[infor[0]]
        for i in range(startpos,endpos,WINDOW_SIZE):
            if i in tempSVs:
                Chroms_windows[i] = tempSVs[i]
            else:
                Chroms_windows[i] = 1
            
SV_Values[lastChrom] = copy.deepcopy(Chroms_windows)

for chrom in sorted(SV_Values.iterkeys()):
    chrom_sv = SV_Values[chrom]
    sorted_sv = sorted(chrom_sv.iterkeys())
    for site in sorted_sv:
        startpos = site+1
        endpos = startpos + WINDOW_SIZE -1
        print ("%s\t%s\t%s\t%s" %(chrom,startpos,endpos,chrom_sv[site]))
    
