#!/usr/bin/env python
import sys
import copy
POPMISSING = 0.34

def findCols(header, pops):
    samplecols = {}
    for pop in pops:
        subpop = pops[pop]
        cols = []
        for sample in subpop:
            if sample not in header:
                sys.stderr.write("Error:cannot find %s in header!\n" %(sample))
                sys.exit(1)
            else:
                cols.append(header.index(sample))
        samplecols[pop] = cols
    return samplecols

def chkMissingCall(matrixrow,samplecols):
    for pop in samplecols:
        popcols = samplecols[pop]
        miscount = 0
        for col in popcols:
            if matrixrow[col] == '.':
                miscount += 1
                #return False ##remove all loci with missing calls
        misrate = miscount/float(len(popcols))
        if misrate > POPMISSING:
            return False
    return True

def readPopInfo(popfile):
    fh_pop = open(popfile, 'rU')
    pops = {}
    popname = ''
    popitems = []
    for line in fh_pop:
        lineinfo = line.strip()
        if lineinfo[0] == '@':
            if popname != '':
                pops[popname] = copy.copy(popitems)
            popname = lineinfo[1:]
            popitems = []
        else:
            popitems.append(lineinfo)
    fh_pop.close()
    pops[popname] = copy.copy(popitems)
    return (pops)

if __name__ == '__main__':
    vcffile = sys.argv[1]
    popfile = sys.argv[2]
    
    pops = readPopInfo(popfile)
    
    with open(vcffile, 'r') as fh_vcf:
        for line in fh_vcf:
            line = line.rstrip()
            if line[0] == "#":
                if line[1] == "#":
                    print line
                else:
                    print line
                    info = line.split('\t')
                    samplecols = findCols(info, pops)
            else:
                info = line.split()[9:]
                if '.' not in info:
                    print line
                else:
                    info = line.split('\t')
                    if chkMissingCall(info, samplecols):
                        print line
    
    
