#!/usr/bin/perl
use strict;

my ($SV_All,$SV_Strain_List,$OutFile)=@ARGV;

my @all_sv;
my @filelst;

my $Flk_len=100;

open(my $fh_svall,"<","$SV_All") or die "Can't open file: $SV_All";
while (<$fh_svall>) {
    chomp();
    my @lines=split(/\t/,$_);
    my %svinfo;
    $svinfo{'chrA'}=$lines[0];
    $svinfo{'chrB'}=$lines[2];
    $svinfo{'posA'}=$lines[1];
    $svinfo{'posB'}=$lines[3];
    push(@all_sv,\%svinfo);
}
close $fh_svall;


open(my $fh_filelst,"<",$SV_Strain_List) or die "Can't open file: $SV_Strain_List";
while (<$fh_filelst>) {
    chomp();
    push(@filelst,$_);
}
close $fh_filelst;

open(my $fh_out,">","$OutFile");
foreach my $SV_Strain(@filelst){
    my @strain_sv;
    open(my $fh_svstrain,"<", "$SV_Strain") or die "Can't open file: $SV_Strain";
    while (<$fh_svstrain>) {
        chomp();
        my @lines=split(/\t/,$_);
        my %svinfo;
        next if ($lines[5] ne "pass");
        $svinfo{'chrA'}=$lines[0];
        $svinfo{'chrB'}=$lines[2];
        $svinfo{'posA'}=$lines[1];
        $svinfo{'posB'}=$lines[3];
        push(@strain_sv,\%svinfo);
    }
    close $fh_svstrain;

    $SV_Strain=~/aCneoH99.r(\S+).DEL|INV|DUP|CTX/;
    my $strainName=$1;
    print $fh_out "$strainName";
    for(my $i=0;$i<@all_sv;$i++){
        my %infoA=%{$all_sv[$i]};
        my $isfound=0;
        for(my $j=0;$j<@strain_sv;$j++){
            my %infoB=%{$strain_sv[$j]};
            if ($infoA{'chrA'} eq $infoB{'chrA'} && $infoA{'chrB'} eq $infoB{'chrB'}) {
                if($infoA{'posA'} - $Flk_len <= $infoB{'posA'} && $infoA{'posA'} + $Flk_len >= $infoB{'posA'}){
                    if ($infoA{'posB'} - $Flk_len <= $infoB{'posB'} && $infoA{'posB'} + $Flk_len >= $infoB{'posB'}) {
                        $isfound=1;
                    }
                }
            }
        }
        if ($isfound==1) {
            print $fh_out "\t1";
        }
        else{
            print $fh_out "\t0";
        }
    }
    print $fh_out "\n";
}
close $fh_out;
exit(0);
