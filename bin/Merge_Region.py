#!/usr/bin/env python

import sys
FileList = sys.argv[1]

if FileList == "":
    print ("Usage:%s <InputFile>" %(sys.argv[0]))

Fh_File = open(FileList,'r')
lastChrom = ""
lastStart = 0
lastEnd = 0
totallength = 0
for line in Fh_File:
    stripped = line.strip()
    infors = stripped.split('\t')
    if lastChrom != infors[0]:
        if lastStart != 0:
            totallength += lastEnd-lastStart+1
            print ("%s\t%s\t%s" %(lastChrom,lastStart,lastEnd))

        lastChrom = infors[0]
        lastStart = int(infors[1])
        lastEnd = int(infors[3])
        
    else:
        if int(infors[1]) > lastEnd:
            print ("%s\t%s\t%s" %(lastChrom,lastStart,lastEnd))
            totallength += lastEnd-lastStart+1
            lastStart = int(infors[1])
            lastEnd = int(infors[3])
        else:
            if int(infors[3]) > lastEnd:
                lastEnd = int(infors[3])
            else:
                pass
            
totallength += lastEnd-lastStart+1
print ("%s\t%s\t%s" %(lastChrom,lastStart,lastEnd))
sys.stderr.write("Total length is: %s\n" %(totallength))


