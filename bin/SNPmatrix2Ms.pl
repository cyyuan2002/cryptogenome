#!/usr/bin/perl

##Input File Format
#Scaffold#       Position        Reference       125.91  A2_102_5        AD1_7a
#supercont2.1    36      T       rh      nd      rh      rh      rh      nd          TC      rh      nd
#supercont2.1    47      C       rh      nd      rh      rh      rh      nd      nd      rh
#supercont2.1    54      T       rh      nd      rh      rh      rh      nd      nd      rh      rh      nd      nd      nd      rh      rh      rh      rh      nd

use strict;

my @ClinicStrains=(3,5,6,7,8,9,10,11,12,13,14,15,16,17,18,22,23,24,25,31,32,33,34,35,36,37,38,39,40,41);
my @EnviroStrains=(4,19,20,26,27,28,29,30);
my $MatrixFile=shift;
open(my $fh_maxfile,"$MatrixFile") or die "$!\n";
my $lastChrom="";
my $fh_clout;
my $fh_evout;
<$fh_maxfile>;
while (my $line=<$fh_maxfile>) {
    chomp($line);
    my @lines=split(/\t/,$line);
    if ($lastChrom ne $lines[0]) {
        close $fh_clout if ($fh_clout ne "");
        close $fh_evout if ($fh_evout ne "");
        my $CloutFile = "clinic.$lines[0].ms";
        my $EvoutFile = "enviro.$lines[0].ms";
        open($fh_clout, ">$CloutFile");
        open($fh_evout, ">$EvoutFile");
        print $fh_clout "position\tx\tn\n";
        print $fh_evout "position\tx\tn\n";
        $lastChrom = $lines[0];
    }
    my $cltotal=0;
    my $clcount=0;
    foreach my $col(@ClinicStrains){
        $cltotal ++ if ($lines[$col] ne "nd");
        $clcount ++ if ($lines[$col] ne "nd" and $lines[$col] ne "rh");
    }
    print $fh_clout "$lines[1]\t$clcount\t$cltotal\n";
    my $evtotal=0;
    my $evcount=0;
    foreach my $col(@EnviroStrains){
        $evtotal ++ if ($lines[$col] ne "nd");
        $evcount ++ if ($lines[$col] ne "nd"  and $lines[$col] ne "rh");
    }
    print $fh_evout "$lines[1]\t$evcount\t$evtotal\n";
}
close $fh_clout;
close $fh_evout;

