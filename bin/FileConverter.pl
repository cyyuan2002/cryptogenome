#!/usr/bin/perl
use strict;

my $file=shift;
open(my $fh_in,"<","$file");
my $outfile="$file.cvt";
open(my $fh_out,">","$outfile");
while (<$fh_in>) {
    chomp();
    my @lines=split(/\t/,$_);
    next if ($lines[5] ne "pass");
    my $chrom=$lines[0];
    $chrom=~s/supercont2./chr/g;
    print $fh_out "$chrom\t$lines[1]\t$lines[3]\t1\n";
}
close $fh_in;
close $fh_out;
