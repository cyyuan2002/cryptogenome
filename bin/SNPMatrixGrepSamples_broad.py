#!/usr/bin/env python
#this script is used to extract SNPs of a group of strains from SNP matrix (Broad version)
import csv
import sys
from math import floor

NonChar = 'nd'

def findcols (header, strains):
    straincols = []
    
    for strain in strains:
        if strain not in header:
            sys.stderr.write("Error:cannot find %s in header!\n" %(strain))
            sys.exit(1)
        else:
            straincols.append(header.index(strain))
    
    return straincols

def convAllele (alcounts, straincols, line):
    alleles = [];
    alcode = [];
    for allele in alcounts:
        if allele != 'nd' and allele != 'rh':
            alleles.append(allele)
    
    for col in straincols:
        if line[col] == 'nd':
            alcode.append('.')
        elif line[col] == 'rh':
            alcode.append('0')
        else:
            alcode.append(str(alleles.index(line[col])+1))
    if len(alleles) == 0:
        alleles.append('.')
    
    for i in xrange(len(alleles)):
        if len(alleles[i]) > 1:
            alleles[i] = alleles[i][0]

    altypes = ','.join(alleles)
    alcodes = '\t'.join(alcode)
    return (altypes, alcodes)

if __name__ == '__main__':
    if len(sys.argv) < 3:
        sys.stderr.write ("Usage:%s <matrix_file> <strain_file> [missing call filter: default 0.2]\n" %(sys.argv[0]))
        sys.exit(1)
    
    fmatrix = sys.argv[1]
    fstrain = sys.argv[2]
    ncratio = 0.5
    
    if len(sys.argv) == 4:
        ncratio = sys.argv[3]
    
    fh_strain = open(fstrain, 'rU')
    strains = [x.strip('\n') for x in fh_strain.readlines()]
    fh_strain.close()
    
    fh_matrix = open(fmatrix, 'rU')
    reader = csv.reader(fh_matrix, delimiter="\t")
    header = reader.next()
    ncnum = ncratio * len(strains)
    straincols = findcols(header, strains)
    print ("#CHROM\tPOS\tREF\tALT\t"+"\t".join(strains))
    for line in reader:
        alcounts = {}
        for col in straincols:
            if(line[col] not in alcounts):
                alcounts[line[col]] = 1
            else:
                alcounts[line[col]] += 1
        if len(alcounts) < 2: #no difference 
            continue
        if len(alcounts) == 2: # only missing data and one allele
            if NonChar in alcounts:
                continue
        if NonChar in alcounts:
            if(alcounts[NonChar] > ncnum):
                continue
        outline = "\t".join(line[0:3])
        altypes, alcodes = convAllele(alcounts,straincols,line)
        outline += "\t" + altypes + "\t" + alcodes
        print outline
