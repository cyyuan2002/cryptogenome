#!/usr/bin/env python
from __future__ import division
import csv
import sys
import copy

def readPopInfo(popfile):
    fh_pop = open(popfile, 'rU')
    pops = {}
    popname = ''
    popitems = []
    for line in fh_pop:
        lineinfo = line.strip()
        if lineinfo[0] == '@':
            if popname != '':
                pops[popname] = copy.copy(popitems)
            popname = lineinfo[1:]
            popitems = []
        else:
            popitems.append(lineinfo)
    fh_pop.close()
    pops[popname] = copy.copy(popitems)
    return (pops)

def findCols(header, pops):
    samplecols = {}
    for pop in pops:
        subpop = pops[pop]
        cols = []
        for sample in subpop:
            if sample not in header:
                sys.stderr.write("Error:cannot find %s in header!\n" %(sample))
                sys.exit(1)
            else:
                cols.append(header.index(sample))
        samplecols[pop] = cols
    return samplecols

def samplecol2samplepop(samplecols):
    samplepops = {}
    for pop in samplecols:
        newcols = []
        samples = samplecols[pop]
        for col in samplecols[pop]:
            newcols.append(col-4)
        samplepops[pop] = newcols
    return samplepops

def calOverPenct(per1, per2):
    overp = 0.0
    if per1 > per2:
        overp = per2/per1
    else:
        overp = per1/per2
    return overp

if __name__ == '__main__':
    if len(sys.argv) < 2:
        sys.stderr.write ("Usage:%s <SNPMatrix> <PopInfo>" %(sys.argv[0]))
        sys.exit(1)
    
    fmatrix = sys.argv[1]
    fpop = sys.argv[2]
    
    windowsize = 10000
    pops = readPopInfo(fpop)
    fh = open(fmatrix, 'rU')
    reader = csv.reader(fh, delimiter = '\t')
    header = reader.next()
    samplecols = findCols(header, pops)
    popNames = sorted(pops.keys())
    popNum = len(popNames)
    outline = "#chome\tpos"
    for i in xrange(len(popNames)):
        for j in xrange(i+1,len(popNames)):
            outline += "\t" + popNames[i]+":"+popNames[j]
    print outline
    
    framecount = 1
    lastchrom = ""
    siterange = framecount * windowsize
    initprec = [0.0, 0.0, 0.0]
    popoverperc = copy.copy(initprec)
    linecount = 0
    
    for line in reader: 
        if lastchrom != line[0]:
            if lastchrom != "":
                windowS =(framecount - 1) * windowsize +  1
                windowE = framecount * windowsize
                outInfo = "%s\t%s-%s" %(lastchrom,windowS,windowE)
                for i in xrange(len(popoverperc)):
                    if linecount > 4:
                        outInfo += "\t%.3f" %(popoverperc[i]/linecount)
                    else:
                        outInfo += "\tNA"
                print outInfo
            lastchrom = line[0]
            popoverperc = copy.copy(initprec)
            framecount = 1
            siterange = framecount * windowsize
            linecount = 0
        if siterange < int(line[1]):
            windowS =(framecount - 1) * windowsize +  1
            windowE = framecount * windowsize
            outInfo = "%s\t%s-%s" %(lastchrom,windowS,windowE)
            for i in xrange(len(popoverperc)):
                if linecount > 4:
                    outInfo += "\t%.3f" %(popoverperc[i]/linecount)
                else:
                    outInfo += "\tNA"
            print outInfo
            popoverperc = copy.copy(initprec)
            framecount += 1
            siterange = framecount * windowsize
            linecount = 0
        linecount += 1
        popalfreq = {}
        altypes = len(line[3].split(',')) + 1
        
        for pop in popNames:  
            typecount = {}
            samplecount = 0
            popcols = samplecols[pop]
            typefreq = {}
            for col in popcols:
                if line[col] !='.':
                    samplecount += 1
                    if line[col] not in typecount:
                        typecount[line[col]] = 1
                    else:
                        typecount[line[col]] += 1
            for altype in typecount:
                typefreq[altype] = typecount[altype]/samplecount
            popalfreq[pop] = typefreq
        
        rescount = 0
        for i in xrange(len(popNames)):
            for j in xrange(i+1,len(popNames)):
                overpercent = 0.0
                pop1 = popalfreq[popNames[i]]
                pop2 = popalfreq[popNames[j]]
                sharecount = 0
                for k in xrange(altypes):
                    if str(k) in pop1 or str(k) in pop2:
                        sharecount += 1
                    if str(k) in pop1 and str(k) in pop2:
                        overpercent += calOverPenct(pop1[str(k)],pop2[str(k)])
                        #overpercent += 1.0
                popoverperc[rescount] += overpercent/sharecount
                rescount += 1
    fh.close()