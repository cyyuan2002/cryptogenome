#!/usr/bin/perl
use strict;
my $UPLENGTH=1000;

sub Dealinfo{
    my $info=shift;
    my @outgenelst;
    my @ingenelst;
    $info =~ /\w+:(\w+) -- (.*)/;
    my $mapregion=$1;
    my $geneinfo=$2;
    if ($mapregion eq "intergenic") {
        $geneinfo =~ /pre_gene\((\d+)\) (\w+)\((\S)\) <\|> next_gene\((\d+)\) (\w+)\((\S)\)/;
        my $predist=$1;
        my $nxtdist=$4;
        my $predir=$3;
        my $nxtdir=$6;
        my $pregene=$2;
        my $nxtgene=$5;
        if ($predist < $UPLENGTH && $predir eq "-") {
            push (@outgenelst,$pregene);
        }
        if ($nxtdist < $UPLENGTH && $nxtdir eq "+") {
            push (@outgenelst,$nxtgene);
        }
    }
    else{
        {
            $geneinfo =~ /(\S+)\(\S\)/;
            my $gene=$1;
            push(@ingenelst,$gene);
        }
    }
    return (\@ingenelst,\@outgenelst);
}


my ($FileName) = shift;
if ($FileName eq "") {
    die "Usage:$0 <File_Name>\n";
}

my $OutFile="$FileName.glist";
my %CTXgene;
my @ingenes;
my @outgenes;

open(my $fh_filein,"$FileName");
while (<$fh_filein>) {
    chomp();
    my @lines=split(/\t/,$_);
    my ($Lingenes,$Loutgenes) = Dealinfo($lines[7]);
    my ($Ringenes,$Routgenes) = Dealinfo($lines[8]);
    push (@ingenes,@{$Lingenes});
    push (@ingenes,@{$Ringenes});
    push (@outgenes,@{$Loutgenes});
    push (@outgenes,@{$Routgenes});
}
close $fh_filein;

my @noreduningenes=keys %{{map {$_ => 1} @ingenes}};
my @noredunoutgenes=keys %{{map {$_ => 1} @outgenes}};

open(my $out_outglist,">$FileName.out.glist");
print $out_outglist join("\n",@noredunoutgenes);
close $out_outglist;

open(my $out_inglist,">$FileName.in.glist");
print $out_inglist join("\n",@noreduningenes);
close $out_inglist;

exit(0);