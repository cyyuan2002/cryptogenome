#!/usr/bin/perl
use strict;

my %VNB=("rcng9" => 1,"rBt85" => 1,"rTu401_1" => 1,"rBt63" => 1,"rZe90_1" => 1,"rcng11"=> 1,"rBt206" => 1,"rBt1" => 1);
my %VN2=("rcng10" => 1,"rMW_RSA852" => 1,"rC45"=> 1);
my %VN1=("rBt15" => 1,"rBt120" => 1,"rA1" => 1,"rH99" => 1,"rC23" => 1,"rAD2_60a" => 1,
         "rcng8" => 1,"rAD1_7a" => 1,"rD17_1" => 1,"rA2_102_5" => 1,"rTu259_1" => 1,"rCHC193" => 1,"rA5" => 1,
         "rMW_RSA1955" => 1,"rC8" => 1,"rAD1_83a" => 1,"rMW_RSA36" => 1,"rGb118" => 1,"rBr795" => 1,"rcng7" => 1,
         "rcng2" => 1,"rcng3" => 1,"rcng6" => 1,"rcng5" => 1,"rcng4" => 1,"rcng1" => 1,"rTh84" => 1,"r125"=> 1);

my $appendix=shift;
my @VNBFiles;
my @VN2Files;
my @VN1Files;

while (<>) {
    chomp();
    my @fileinfo=split(/\./,$_);
    if (exists($VNB{$fileinfo[1]})) {
        push (@VNBFiles,$_);
    }
    elsif(exists($VN2{$fileinfo[1]})){
        push (@VN2Files,$_);
    }
    elsif(exists($VN1{$fileinfo[1]})){
        push (@VN1Files,$_);
    }
}

open(my $fh_vn1, ">", "VNI.lst");
print $fh_vn1 join("\n",@VN1Files);
print $fh_vn1 "\n";
close $fh_vn1;
open(my $fh_vn2, ">", "VNII.lst");
print $fh_vn2 join("\n",@VN2Files);
print $fh_vn2 "\n";
close $fh_vn2;
open(my $fh_vnb, ">", "VNB.lst");
print $fh_vnb join("\n",@VNBFiles);
print $fh_vnb "\n";
close $fh_vnb;

