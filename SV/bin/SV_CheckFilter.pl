#!/usr/bin/perl
use strict;

my ($inputfile,$mode) = @ARGV;
if (@ARGV < 2) {
    die "Usage:$0 INPUT_FILE MODE (0: assembly, 1: refgenome, 2: CTX)";
}

open(my $fh_in,"$inputfile") or die "Can't open file:$inputfile";
while (<$fh_in>) {
    chomp();
    my @lines=split(/\t/);
    if ($mode == 0) {
        if ($lines[5] eq "pass") {
            print "$lines[0]\t$lines[1]\t$lines[2]\t$lines[3]\t$lines[4]\n";
        }
    }
    elsif($mode == 1){
        if ($lines[0] eq "Varified") {
            print "$lines[1]\t$lines[2]\t$lines[3]\t$lines[4]\t$lines[5]\n";
        }   
    }
    if ($mode == 2) {
        if ($lines[6] eq "pass") {
            print "$lines[0]\t$lines[1]\t$lines[2]\t$lines[3]\t$lines[4]\t$lines[7]\n";
        }
    }
    
}

close $fh_in;
exit(0)
