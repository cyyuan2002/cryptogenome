#!/usr/bin/env python
import sys
import csv
import os
import tempfile

if(len(sys.argv) < 3):
    print ("Usage: <DEL_FILE> <DUP_FILE>")
    sys.exit(1)

Del_File = sys.argv[1]
Dup_File = sys.argv[2]

tmpFile = tempfile.NamedTemporaryFile('w')
tmpName = tmpFile.name

with open(Del_File,'r') as csvDel:
    reader = csv.reader(csvDel,delimiter="\t")
    for line in reader:
        delline = "\t".join(line[0:5])
        tmpFile.write(delline + "\n")
    tmpFile.flush()

with open(Dup_File,'r') as csvDup:
    reader = csv.reader(csvDup,delimiter="\t")
    for line in reader:
        dupline = "\t".join(line[0:6])
        tmpFile.write(dupline + "\n")
    tmpFile.flush()
    
print (os.popen("cat "+ tmpName + " | sort -k 1,1 -k 2,2n -k 3,3 -k 4,4n" ).read(),end="")

tmpFile.close()
sys.exit(0)
