#!/usr/bin/env python
"""This script is used to search genes in provides regions \
based on GTF/GFF file. The input format is: \
'chromsome  startpos   endpos'."""

import csv
import sys

class AnnoRec(object):
    def __init__(self,chrom,start,end,length):
        self.chrom = chrom
        self.start = start
        self.end = end
        self.length = length

OVER_PERCENT = 20

if(len(sys.argv) < 3):
    print ("Usage:%s <Region_File> <GFF/GTF_File>" %(sys.argv[0]))
    sys.exit(1)

Region_File = sys.argv[1]
GFF_File = sys.argv[2]

info_limits = dict(gff_type=["gene"])

GeneAnnos = {}

fh_gff = open(GFF_File,'r')
gff_reader = csv.reader(fh_gff,delimiter = "\t")
for rec in gff_reader:
    if(len(rec) < 1):  #skip empty line
        continue
    if(rec[2] == 'gene'):
        start = int(rec[3])
        end = int(rec[4])
        chrom = rec[0]
        geneID = rec[8][3:13]
        length = end - start + 1
        anno = AnnoRec(chrom, start, end,length)
        GeneAnnos[geneID] = anno
fh_gff.close()

fh_infile = open(Region_File,'r')
region_reader = csv.reader(fh_infile,delimiter = "\t")
for line in region_reader:
    start = int(line[1])
    end = int(line[2])
    region_length = end - start
    region_genes = []
    for geneID in GeneAnnos:
        rec = GeneAnnos[geneID]
        if(rec.chrom != line[0]):
            continue
        overlength = 0
        if(start <= rec.start and end >= rec.start):
            if(end >= rec.end):
                overlength = rec.length
            else:
                overlength = end - rec.start + 1
        elif(start <= rec.end and end >= rec.end):
            overlength = rec.end - start + 1
        elif(start >= rec.start and end <= rec.end):
            overlength = end - start + 1
        if(overlength/rec.length * 100 >= OVER_PERCENT):
            region_genes.append(geneID)
    if(len(region_genes) > 0):
        print ("\t".join(line) + "\t" + ",".join(region_genes))
fh_infile.close()
sys.exit(0)
    