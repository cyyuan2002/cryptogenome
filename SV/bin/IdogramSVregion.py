#!/usr/bin/env python
"""This script is used to merge the region for drawing \
the ideogram of heatmap"""
import sys

WINDOW_SIZE = 500
MINOVER = 0.2

InFile = sys.argv[1]
fh_file = open(InFile,'r')

Chroms_windows = {}
SV_Values = {}

for line in fh_file:
    striped = line.strip()
    infor = striped.split('\t')

    startpos = (int(infor[1])//WINDOW_SIZE)*WINDOW_SIZE
    endpos = (int(infor[2])//WINDOW_SIZE+1)*WINDOW_SIZE

    if (int(infor[1]) - startpos) >= WINDOW_SIZE * (1-MINOVER):
        startpos = startpos + WINDOW_SIZE

    if (int(infor[2]) - endpos) >= WINDOW_SIZE * MINOVER:
        endpos = endpos - 2 *WINDOW_SIZE
    else:
        endpos = endpos - WINDOW_SIZE

    print ("%s\t%s\t%s" %(infor[0],startpos,endpos))

sys.exit(0)
