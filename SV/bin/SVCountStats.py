#!/usr/bin/env python

import csv
import sys
import re

Files = sys.argv[1:]
for file in Files:
    fh_File = open(file, 'r')
    reader = csv.reader(fh_File, delimiter = "\t")
    SVcount = 0
    SVlength = 0
    genecount = 0
    for line in reader:
        totalline = ",".join(line)
        SVcount += 1
        SVlength += int(line[3]) - int(line[1])
        cds = re.findall(',CDS:(\d+),', totalline)
        genecount += int(cds[0])
    if(SVcount == 0):
        avglength = 0
    else:
        avglength = SVlength//SVcount
    strain = re.findall('aCneoH99.r(\S+).(?:del|dup)', file)
    print ("%s\t%s\t%s\t%s\t%s" %(strain[0], SVcount, SVlength,avglength,genecount))

