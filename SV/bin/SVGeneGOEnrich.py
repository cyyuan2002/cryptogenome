#!/usr/bin/env python
import sys
import csv
BIN_PATH = "/Users/Alvin/Workspace/Dropbox/Python/Anno/"
sys.path.append(BIN_PATH)
import GOEnrich

if(len(sys.argv)<3):
    print ("Usage: %s <Infile> <GOTable>" %(sys.argv[0]))
    exit(1)

Infile = sys.argv[1]
GOTable = sys.argv[2]

fh_infile = open(Infile, 'r')
reader_in = csv.reader(fh_infile, delimiter = '\t')
genelist = []
for line in reader_in:
    genes = line[3].split(',')
    genelist.extend(genes)
fh_infile.close()

fh_GOtable = open(GOTable, 'r')
allgenelist = []
reader_GO = csv.reader(fh_GOtable, delimiter = '\t')
for line in reader_GO:
    geneid = line[0]
    allgenelist.append(geneid)
fh_GOtable.close()

GOEnrich.goenrich(genelist,allgenelist,GOTable,Infile)

sys.exit(0)
